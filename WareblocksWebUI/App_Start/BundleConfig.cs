﻿using System.Web.Optimization;

namespace WareblocksWebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryajax").Include(
                "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/Scripts/jquery-ui-1.11.4.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/materialize").Include(
                "~/Scripts/materialize/materialize.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/materialize/font/css").Include(
                "~/Content/materialize/materialize.css"));

            bundles.Add(new ScriptBundle("~/bundles/scroll").Include(
                "~/Scripts/jquery.panelSnap.js"));

            //NEW

            bundles.Add(new StyleBundle("~/Content/theme/css").Include(
                "~/Content/theme/bootstrap.css",
                "~/Content/theme/bootstrap.css",
                "~/Content/theme/materialadmin.css",
                "~/Content/theme/font-awesome.min.css",
                "~/Content/theme/material-design-iconic-font.min.css",
                "~/Content/theme/jquery.modal.css"));

            bundles.Add(new ScriptBundle("~/Scripts/materialbase").Include(
                "~/Scripts/libs/jquery/jquery-1.11.2.min.js", "~/Scripts/libs/jquery/jquery-migrate-1.2.1.min.js",
                "~/Scripts/libs/bootstrap/bootstrap.min.js", "~/Scripts/libs/nanoscroller/jquery.nanoscroller.min.js", "~/Scripts/jquery.modal.js"
                ));

            bundles.Add(new ScriptBundle("~/Scripts/materialapp").Include("~/Scripts/App.min.js"));
        }
    }
}