﻿using AutoMapper;
using DomainDTO.Implementations;
using WareblocksWebUI.Models;
using WareblocksWebUI.Models.Account;
using WareblocksWebUI.Models.Comment;
using WareblocksWebUI.Models.Feature;
using WareblocksWebUI.Models.Idea;
using WareblocksWebUI.Models.Project;
using WareblocksWebUI.Models.Vote;

namespace WareblocksWebUI.App_Start
{
    public static class MapperConfig
    {
        public static void InitializeMapper()
        {
            #region Account

            Mapper.CreateMap<RegisterViewModel, UserDto>();
            Mapper.CreateMap<LoginViewModel, UserDto>();

            #endregion Account

            #region Project

            Mapper.CreateMap<ProjectDto, ProjectViewModel>();
            Mapper.CreateMap<ProjectViewModel, ProjectDto>();

            #endregion Project

            Mapper.CreateMap<FeatureModel, FeatureDto>();
            Mapper.CreateMap<FeatureDto, FeatureModel>().ForMember(x => x.ProjectId, (y) => y.MapFrom(x => x.Project.Key));

            Mapper.CreateMap<IdeaModel, IdeaDto>();
            Mapper.CreateMap<IdeaDto, IdeaModel>().ForMember(model => model.ProjectId, a => a.MapFrom(dto => dto.Project.Key));

            Mapper.CreateMap<CommentModel, CommentDto>();
            Mapper.CreateMap<CommentDto, CommentModel>()
                .ForMember(model => model.ProjectId, a => a.MapFrom(dto => dto.Project.Key))
                .ForMember(model => model.IdeaId, a => a.MapFrom(dto => dto.Idea.Key))
                .ForMember(model => model.FeatureId, a => a.MapFrom(dto => dto.Feature.Key));

            Mapper.CreateMap<VoteDto, VoteModel>().ReverseMap();

            Mapper.CreateMap<SubscribeBetaModel, SubscribeBetaDto>().ReverseMap();
        }
    }
}