﻿using Application.Core;
using AutoMapper;
using System;
using System.Web.Mvc;
using WareblocksWebUI.Models;
using WareblocksWebUI.Models.Vote;

namespace WareblocksWebUI.Controllers
{
    [Authorize]
    public class VoteController : Controller
    {
        public ApplicationEntry Application { get; set; }

        public VoteController()
            : this(new ApplicationEntry())
        {
        }

        private VoteController(ApplicationEntry applicationEntry)
        {
            Application = applicationEntry;
        }

        // GET: Vote
        public ActionResult VoteViewForFeature(Guid userId, string featureId)
        {
            var featureVote = Application.GetFeatureVote(userId, (ShortGuid)featureId);
            if (featureVote == null)
                return PartialView(new VoteModel { ExternalId = featureId });

            var voteModel = Mapper.Map<VoteModel>(featureVote);
            voteModel.ExternalId = featureId;
            return PartialView(voteModel);
        }
    }
}