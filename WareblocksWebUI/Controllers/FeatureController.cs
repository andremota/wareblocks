using Application.Core;
using AutoMapper;
using DomainDTO.Implementations;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WareblocksWebUI.Models;
using WareblocksWebUI.Models.Comment;
using WareblocksWebUI.Models.Feature;

namespace WareblocksWebUI.Controllers
{
    [Authorize]
    public class FeatureController : Controller
    {
        public ApplicationEntry Application { get; set; }

        public FeatureController()
            : this(new ApplicationEntry())
        {
        }

        private FeatureController(ApplicationEntry applicationEntry)
        {
            Application = applicationEntry;
        }

        // GET: Feature
        public ActionResult Index(string featureId)
        {
            var projectFeatureDto = new FeatureDto { Key = (ShortGuid)featureId };
            var result = Application.Get(projectFeatureDto);
            
            var projectFeatureModel = Mapper.Map<FeatureDto, FeatureModel>(result);
            
            return PartialView(projectFeatureModel);
        }

        public ActionResult Create(string projectId)
        {
            return PartialView(new FeatureModel { ProjectId = ((ShortGuid)projectId) });
        }

        [HttpPost]
        public ActionResult Create(FeatureModel createModel)
        {
            var projectFeatureDto = Mapper.Map<FeatureModel, FeatureDto>(createModel);
            projectFeatureDto.Project = new ProjectDto { Key = (ShortGuid)createModel.ProjectId };

            Application.Insert(projectFeatureDto);

            return RedirectToAction("Index", "Project", new { id = (ShortGuid)createModel.ProjectId });
        }

        [HttpGet]
        public ActionResult GetFeaturePopup(string featureId)
        {
            var projectFeatureDto = new FeatureDto { Key = (ShortGuid)featureId };
            var result = Application.Get(projectFeatureDto);
            return View(result);
        }

        public ActionResult FeaturesByProject(string projectId)
        {
            var result = Application.GetAllFeaturesFromProject(new ProjectDto { Key = (ShortGuid)projectId });
            var allFeaturesFromProject = Mapper.Map<IEnumerable<FeatureDto>, IEnumerable<FeatureModel>>(result);
            var viewModel = new FeatureViewModel { Features = allFeaturesFromProject, ProjectId = projectId };
            return PartialView(viewModel);
        }

        public ActionResult GetPaymentPopup()
        {
            return PartialView("Payment", new PaymentModel());
        }

        public ActionResult GetPaymentResult()
        {
            return PartialView("PaymentResult");
        }

        [HttpPost]
        public ActionResult Vote(string featureId, bool isLike)
        {
            var featureGuid = (ShortGuid)featureId;
            var userId = Guid.Parse(User.Identity.GetUserId());
            Application.VoteFeature(featureGuid, userId, isLike);
            return null;
        }
    }
}