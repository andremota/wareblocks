﻿using Application.Core;
using AutoMapper;
using DomainDTO.Implementations;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WareblocksWebUI.Models;
using WareblocksWebUI.Models.Comment;
using WareblocksWebUI.Models.Idea;

namespace WareblocksWebUI.Controllers
{
    [Authorize]
    public class IdeaController : Controller
    {
        public ApplicationEntry Application { get; set; }

        public IdeaController()
            : this(new ApplicationEntry())
        {
        }

        private IdeaController(ApplicationEntry applicationEntry)
        {
            Application = applicationEntry;
        }

        public ActionResult Index(string ideaId)
        {
            var ideaDto = new IdeaDto() { Key = (ShortGuid)ideaId };
            
            var result = Application.Get(ideaDto);
            var projectFeatureModel = Mapper.Map<IdeaModel>(result);

            return View(projectFeatureModel);
        }

        [HttpPost]
        public ActionResult Create(IdeaModel createModel)
        {
            var toCreate = Mapper.Map<IdeaDto>(createModel);
            toCreate.Project = new ProjectDto() { Key = (ShortGuid)createModel.ProjectId };

            Application.Insert(toCreate);

            return RedirectToAction("Index", "Project", new { id = (ShortGuid)createModel.ProjectId });
        }

        public ActionResult Create(string projectId)
        {
            return PartialView(new IdeaModel { ProjectId = ((ShortGuid)projectId) });
        }

        public ActionResult IdeasByProject(string projectId)
        {
            var result = Application.GetAllIdeasFromProject(new ProjectDto { Key = (ShortGuid)projectId });

            var ideasViewModel = new IdeaViewModel()
            {
                Ideas = Mapper.Map<IEnumerable<IdeaModel>>(result),
                ProjectId = projectId
            };

            return PartialView(ideasViewModel);
        }
    }
}