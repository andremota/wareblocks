﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Application.Core;
using AutoMapper;
using DomainDTO.Implementations;
using Microsoft.AspNet.Identity;
using WareblocksWebUI.Models;
using WareblocksWebUI.Models.Comment;

namespace WareblocksWebUI.Controllers
{
    public class CommentController : Controller
    {
        public ApplicationEntry Application { get; set; }

        public CommentController()
            : this(new ApplicationEntry())
        {
        }

        private CommentController(ApplicationEntry applicationEntry)
        {
            Application = applicationEntry;
        }

        public ActionResult GetCommentsForProject(string projectId)
        {
            var projectGuid = (ShortGuid)projectId;

            var comments = Application.GetCommentsForProject(new ProjectDto {Key = projectGuid});

            return PartialView("Comments", new CommentViewModel {Comments = Mapper.Map<IEnumerable<CommentDto>, IEnumerable<CommentModel>>(comments)});
        }

        public ActionResult GetCommentsForFeature(string featureId)
        {
            var featureGuid = (ShortGuid)featureId;

            var comments = Application.GetCommentsForFeature(new FeatureDto { Key = featureGuid });

            return PartialView("Comments", new CommentViewModel { Comments = Mapper.Map<IEnumerable<CommentDto>, IEnumerable<CommentModel>>(comments) });
        }

        public ActionResult GetCommentsForIdea(string ideaId)
        {
            var ideaGuid = (ShortGuid)ideaId;

            var comments = Application.GetCommentsForIdea(new IdeaDto { Key = ideaGuid });

            return PartialView("Comments", new CommentViewModel { Comments = Mapper.Map<IEnumerable<CommentDto>, IEnumerable<CommentModel>>(comments) });
        }

        [HttpPost]
        public ActionResult AddComment(CommentModel createModel)
        {
            var createDto = Mapper.Map<CommentModel, CommentDto>(createModel);

            createDto.User = new UserDto { Id = new Guid(User.Identity.GetUserId()) };

            if (createModel.ProjectId != null)
            {
                createDto.Project = new ProjectDto {Key = (ShortGuid) createModel.ProjectId};
                Application.AddCommentToProject(createDto);
                return GetCommentsForProject(createModel.ProjectId);
            }   

            if (createModel.FeatureId != null)
            {
                createDto.Feature = new FeatureDto { Key = (ShortGuid)createModel.FeatureId };
                Application.AddCommentToFeature(createDto);
                return GetCommentsForFeature(createModel.FeatureId);
            }

            if (createModel.IdeaId != null)
            {
                createDto.Idea = new IdeaDto { Key = (ShortGuid)createModel.IdeaId };
                Application.AddCommentToIdea(createDto);
                return GetCommentsForIdea(createModel.IdeaId);
            }

            return null;
        }
    }
}