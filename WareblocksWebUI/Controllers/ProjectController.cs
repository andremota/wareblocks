﻿using Application.Core;
using AutoMapper;
using DomainDTO.Implementations;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WareblocksWebUI.Models;
using WareblocksWebUI.Models.Comment;
using WareblocksWebUI.Models.Project;

namespace WareblocksWebUI.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        public ApplicationEntry Application { get; set; }

        public ProjectController()
            : this(new ApplicationEntry())
        {
        }

        private ProjectController(ApplicationEntry applicationEntry)
        {
            Application = applicationEntry;
        }

        // GET: Project
        public ActionResult Index(string id)
        {
            var shortId = (ShortGuid)id;
            var projectDto = new ProjectDto { Key = shortId };

            var result = Application.Get(projectDto);
            var projectViewModel = Mapper.Map<ProjectDto, ProjectViewModel>(result);
            
            return View(projectViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return PartialView(new ProjectViewModel());
        }

        [HttpPost]
        public ActionResult Create(ProjectViewModel createModel)
        {
            var createDto = Mapper.Map<ProjectViewModel, ProjectDto>(createModel);

            var createdProject = Application.Insert(createDto, new UserDto() { Id = new Guid(User.Identity.GetUserId()) });
            return RedirectToAction("Index", "Project", new { id = (ShortGuid)createdProject.Key });
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var userDto = new UserDto {Id = new Guid(User.Identity.GetUserId())};
            var projects = Application.GetUserProjects(userDto);
            var projectistDto = Mapper.Map<IEnumerable<ProjectDto>, IEnumerable<ProjectViewModel>>(projects);
            return PartialView("ProjectList", projectistDto);
        }

    }
}