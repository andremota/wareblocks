﻿namespace WareblocksWebUI.Models.Idea
{
    public class IdeaModel
    {
        public ShortGuid Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PositiveVotes { get; set; }
        public int NegativeVotes { get; set; }
        public string ProjectId { get; set; }
    }
}