﻿using System.Collections.Generic;
using WareblocksWebUI.Models.Comment;

namespace WareblocksWebUI.Models.Idea
{
    public class IdeaViewModel
    {
        public IEnumerable<IdeaModel> Ideas { get; set; }
        public string ProjectId { get; set; }
    }
}