﻿using System.Collections.Generic;

namespace WareblocksWebUI.Models.Comment
{
    public class CommentViewModel
    {
        public IEnumerable<CommentModel> Comments { get; set; } 
    }
}