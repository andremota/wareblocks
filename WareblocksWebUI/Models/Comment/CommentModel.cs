﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WareblocksWebUI.Models.Comment
{
    public class CommentModel
    {
        public string Content { get; set; }

        public string ProjectId { get; set; }

        public string IdeaId { get; set; }

        public string FeatureId { get; set; }
    }
}