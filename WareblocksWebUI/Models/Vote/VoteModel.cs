﻿using System;

namespace WareblocksWebUI.Models.Vote
{
    public class VoteModel
    {
        public Guid Key { get; set; }

        public ShortGuid ExternalId { get; set; }

        public bool IsLike { get; set; }
    }
}