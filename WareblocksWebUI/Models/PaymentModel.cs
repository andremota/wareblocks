﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace WareblocksWebUI.Models
{
    public class PaymentModel
    {
        public string Email { get; set; }

        [DisplayName("Credit Card Number")]
        public String CardNumber { get; set; }

        [DisplayName("Name (as on Card)")]
        public String CardName { get; set; }

        [DisplayName("CVV")]
        public String SecurityCode { get; set; }

        public String SelectedMonth { get; set; }

        public IEnumerable<SelectListItem> ExpirationMonthDropdown
        {
            get
            {
                return Enumerable.Range(1, 12).Select(x =>

                    new SelectListItem()
                    {
                        Text = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[x - 1] + " (" + x + ")",
                        Value = x.ToString()
                    });
            }
        }

        public String SelectedYear { get; set; }

        public IEnumerable<SelectListItem> ExpirationYearDropdown
        {
            get
            {
                return Enumerable.Range(DateTime.Today.Year, 20).Select(x =>

                new SelectListItem()
                {
                    Text = x.ToString(),
                    Value = x.ToString()
                });
            }
        }
    }
}