﻿using DomainDTO.Enums;

namespace WareblocksWebUI.Models.Feature
{
    public class FeatureModel
    {
        public FeatureType Type { get; set; }

        public ShortGuid Key { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Budget { get; set; }

        public decimal CurrentBudget { get; set; }

        public string ProjectId { get; set; }
    }
}