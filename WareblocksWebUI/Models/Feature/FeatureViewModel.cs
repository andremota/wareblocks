﻿using System.Collections.Generic;
using WareblocksWebUI.Models.Comment;

namespace WareblocksWebUI.Models.Feature
{
    public class FeatureViewModel
    {
        public ShortGuid ProjectId { get; set; }

        public IEnumerable<FeatureModel> Features { get; set; }
    }
}