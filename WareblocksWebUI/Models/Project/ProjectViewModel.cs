﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WareblocksWebUI.Models.Comment;

namespace WareblocksWebUI.Models.Project
{
    public class ProjectViewModel
    {
        public ShortGuid Key { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public decimal TotalFinanced { get; set; }

        public string ClientId { get; set; }

        public string ClientName { get; set; }

        public string ClientSecretHash { get; set; }

    }
}