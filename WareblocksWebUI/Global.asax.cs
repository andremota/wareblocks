﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WareblocksWebUI.App_Start;

namespace WareblocksWebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleTable.EnableOptimizations = false;
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MapperConfig.InitializeMapper();
        }
    }
}