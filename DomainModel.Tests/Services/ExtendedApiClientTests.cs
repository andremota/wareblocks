﻿using System;
using System.Linq;
using DomainModel.Implementations.Services;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace DomainModel.Tests.Services
{
    //[TestFixture]
    public class ExtendedApiClientTests
    {
        readonly static ExtendedWareblocksApiClient Client = new ExtendedWareblocksApiClient("1", "2", "http://localhost:3000");

        //[Test]
        public void Get_Permissions_Of_An_User_That_Does_Not_Exist_On_The_Api()
        {
            const string userId = "does_not_exist_user_id";
            var user = Client.GetUserPermissionsAsync(userId).Result;
            Assert.That(user.Permissions, Is.Empty);
            Assert.That(user.UserId, Is.EqualTo(userId));
        }

        //[Test]
        public void Get_A_Project_From_The_Api_Using_The_Extended_Client()
        {
            var test = Client.GetProject("1").Result;
            Assert.That(test.id.Value, Is.EqualTo("1"));
            Assert.That(test.key.Value, Is.EqualTo("2"));
        }

        //[Test]
        public void Test_Project_Operations_Of_The_Api()
        {
            var toCreate = new {id = "2", key = "3"};

            // Create
            Client.CreateProject(toCreate.id, toCreate.key).Wait(1000);

            // Get
            var get = Client.GetProject(toCreate.id).Result;
            Assert.That(get.id.Value, Is.EqualTo(toCreate.id));
            Assert.That(get.key.Value, Is.EqualTo(toCreate.key));

            // Remove
            Client.RemoveProject(toCreate.id).Wait(1000);

            // Assert that it does not exist.
            Assert.Throws<AggregateException>(() => Client.GetProject(toCreate.id).Wait(1000));
        }

        //[Test]
        public void Test_ProjectFeature_Operations()
        {
            var toCreate = new { projectId = "1", featureId = "1" };

            // Create
            Client.CreateOrUpdateProjectFeatureAsync(toCreate.projectId, toCreate.featureId, Enumerable.Empty<string>()).Wait();

            // Get
            var created = Client.GetProjectFeature(toCreate.projectId, toCreate.featureId).Result;
            Assert.That(created.projectId.Value, Is.EqualTo(toCreate.projectId));
            Assert.That(created.featureId.Value, Is.EqualTo(toCreate.featureId));

            // Remove
           Client.RemoveProjectFeature(toCreate.projectId, toCreate.featureId).Wait(1000);

           // Assert that it does not exist.
           Assert.Throws<AggregateException>(() => Client.GetProjectFeature(toCreate.projectId, toCreate.featureId).Wait(1000));
        }
    }
}
