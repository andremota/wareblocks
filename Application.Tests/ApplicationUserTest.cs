﻿using DomainModel.Configs.IOC;
using DomainModel.Exceptions;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using Ninject;
using NUnit.Framework;

namespace Application.Tests
{
    [TestFixture]
    [Category("Application: User")]
    class ApplicationUserTest
    {
        private IGateway Gateway { get; set; }

        [SetUp]
        public void Init()
        {
            Gateway = new StandardKernel(new BootstrapModule()).Get<IGateway>();
        }

        [Test]
        public void When_loggin_in_the_user_should_be_matched()
        {
            
        }

        [Test]
        public void When_registering_the_user_the_claims_should_not_be_null()
        {

        }

        [Test]
        [ExpectedException(typeof(MissingModelException<User>))]
        public void When_creating_a_user_it_should_throw_exception_if_no_user_was_provided()
        {
            Gateway.Repositories.User.Insert(null);
        }

        [Test]
        public void When_editing_a_user_should_be_mapped_correctly()
        {

        }

        [Test]
        public void When_deleting_a_user_should_return_true()
        {

        } 
    }
}