﻿using Application.Core;
using DomainModel.Configs.IOC;
using DomainModel.Exceptions;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using Ninject;
using NUnit.Framework;

namespace Application.Tests
{
    [TestFixture]
    [Category("Application: Feature")]
    class ApplicationFeatureTest
    {
        private IGateway Gateway { get; set; }

        private ApplicationEntry Application { get; set; }

        [SetUp]
        public void Init()
        {
            Gateway = new StandardKernel(new BootstrapModule()).Get<IGateway>();
            Application = new ApplicationEntry();
        }

        [Test]
        public void When_getting_a_feature_should_be_mapped_correctly()
        {

        }

        [Test]
        [ExpectedException(typeof(MissingModelException<Feature>))]
        public void When_creating_a_feature_it_should_throw_exception_if_no_feature_was_provided()
        {
            Gateway.Repositories.Feature.Insert(null);
        }

        [Test]
        public void When_creating_a_feature_should_be_mapped_correctly()
        {

        }

        [Test]
        public void When_editing_a_feature_should_be_mapped_correctly()
        {

        }

        [Test]
        public void When_deleting_a_feature_should_return_true()
        {

        } 

        [Test]
        public void When_generating_tokens_it_should_not_return_false()
        {
            Assert.True(Application.GenerateTokens());
        }

        [Test]
        public void When_adding_batch_tokens_to_feature_should_return_true()
        {

        }

        [Test]
        public void When_sending_tokens_to_api_an_api_call_should_be_made()
        {

        } 
    }
}