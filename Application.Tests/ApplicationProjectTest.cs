﻿using DomainModel.Configs.IOC;
using DomainModel.Exceptions;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using Ninject;
using NUnit.Framework;

namespace Application.Tests
{
    [TestFixture]
    [Category("Application: Project")]
    class ApplicationProjectTest
    {
        private IGateway Gateway { get; set; }

        [SetUp]
        public void Init()
        {
            Gateway = new StandardKernel(new BootstrapModule()).Get<IGateway>();
        }

        [Test]
        public void When_getting_a_project_should_be_mapped_correctly()
        {

        }

        [Test]
        [ExpectedException(typeof(MissingModelException<Project>))]
        public void When_creating_a_project_it_should_throw_exception_if_no_project_was_provided()
        {
            Gateway.Repositories.Project.Insert(null);
        }

        [Test]
        public void When_creating_a_project_should_be_mapped_correctly()
        {
            
        }

        [Test]
        public void When_editing_a_project_should_be_mapped_correctly()
        {

        }

        [Test]
        public void When_deleting_a_project_should_return_true()
        {

        } 

    }
}