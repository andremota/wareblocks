﻿using AutoMapper;
using DomainDTO.Implementations;
using DomainModel.Implementations.Entities;
using System;

namespace Application.Configs
{
    public static class MapperInitializer
    {
        /// <summary>
        /// Automapper initialization
        /// </summary>
        public static void InitializeMapper()
        {
            Mapper.CreateMap<Idea, IdeaDto>();
            Mapper.CreateMap<IdeaDto, Idea>()
                .ForMember(u => u.Key, e => e.MapFrom(u => u.Key == Guid.Empty ? Guid.NewGuid() : u.Key));

            //**ENTITY to DTO Maps**
            Mapper.CreateMap<Project, ProjectDto>();
            Mapper.CreateMap<Feature, FeatureDto>().ForMember(x => x.Project, y => y.MapFrom(x => x.Project));
            Mapper.CreateMap<Permission, PermissionDto>();
            Mapper.CreateMap<Api, ApiDto>();
            Mapper.CreateMap<Image, ImageDto>();
            Mapper.CreateMap<UserFinancePhase, UserFinancePhaseDto>();
            Mapper.CreateMap<Token, TokenDto>();
            Mapper.CreateMap<Comment, CommentDto>();

            //**DTO to ENTITY Maps**
            Mapper.CreateMap<ProjectDto, Project>();
            Mapper.CreateMap<FeatureDto, Feature>();
            Mapper.CreateMap<PermissionDto, Permission>();
            Mapper.CreateMap<ApiDto, Api>();
            Mapper.CreateMap<ImageDto, Image>();
            Mapper.CreateMap<UserFinancePhaseDto, UserFinancePhase>();
            Mapper.CreateMap<TokenDto, Token>();
            Mapper.CreateMap<CommentDto, Comment>()
                .ForMember(u => u.Key, e => e.MapFrom(u => u.Key == Guid.Empty ? Guid.NewGuid() : u.Key));
            Mapper.CreateMap<VoteDto, Vote>().ReverseMap();

            #region Account

            Mapper.CreateMap<UserDto, User>()
                .ForMember(u => u.Email, e => e.MapFrom(u => u.UserName))
                .ForMember(u => u.Id, e => e.MapFrom(u => u.Id == Guid.Empty ? Guid.NewGuid() : u.Id));
            Mapper.CreateMap<User, UserDto>();
            Mapper.CreateMap<SubscribeBetaDto, SubscribeBeta>().ReverseMap();

            #endregion Account
        }
    }
}