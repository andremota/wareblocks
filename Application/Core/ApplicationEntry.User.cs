﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DomainDTO.Implementations;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Application.Core
{
    public partial class ApplicationEntry
    {
        public IEnumerable<UserDto> GetAllUsers()
        {
            return Gateway.Repositories.User.Query().Project().To<UserDto>();
        }

        public UserDto GetById(string id)
        {
            var result = Gateway.Repositories.User.Query().FirstOrDefault(u => u.Id == id);
            return result == null ? null : Mapper.Map<UserDto>(result);
        }

        public UserDto Edit(UserDto user)
        {
            var model = Mapper.Map<User>(user);
            model.EnsureNotNull(new ArgumentNullException("user", "User not valid, cannot be null"));

            var result = Gateway.Repositories.User.Update(model);
            return result != null ? Mapper.Map<UserDto>(result) : null;
        }

        public bool Delete(UserDto user)
        {
            var model = Mapper.Map<User>(user);
            model.EnsureNotNull(new ArgumentNullException("user", "User feature not valid"));

            return Gateway.Repositories.User.Delete(model);
        }

        public async Task<ClaimsIdentity> Register(UserDto dto)
        {
            var model = Mapper.Map<User>(dto);

            var inserted = await Gateway.Repositories.User.InsertAsync(model);
            if (inserted != null)
                return await Gateway.Repositories.User.GetUserIdentityAsync(inserted);

            return null;
        }

        public async Task<ClaimsIdentity> MatchUser(UserDto dto)
        {
            var model = Mapper.Map<User>(dto);

            var found = await Gateway.Repositories.User.MatchUser(model);

            if (found != null)
                return await Gateway.Repositories.User.GetUserIdentityAsync(found);

            return null;
        }
    }
}