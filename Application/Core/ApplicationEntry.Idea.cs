﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DomainDTO.Implementations;
using DomainModel.Implementations.Entities;

namespace Application.Core
{
    public partial class ApplicationEntry
    {
        public IdeaDto Insert(IdeaDto idea)
        {
            var toCreate = Mapper.Map<Idea>(idea);
            var created = Gateway.Repositories.Project.InsertIdea(toCreate);
            return Mapper.Map<IdeaDto>(created);
        }

        public IdeaDto Get(IdeaDto idea)
        {
            var toCreate = Mapper.Map<Idea>(idea);
            var created = Gateway.Repositories.Idea.Get(toCreate);
            return Mapper.Map<IdeaDto>(created);
        }

        public IdeaDto Edit(IdeaDto idea)
        {
            var toCreate = Mapper.Map<Idea>(idea);
            var created = Gateway.Repositories.Idea.Update(toCreate);
            return Mapper.Map<IdeaDto>(created);
        }

        public bool Delete(IdeaDto idea)
        {
            var toCreate = Mapper.Map<Idea>(idea);
            return Gateway.Repositories.Idea.Delete(toCreate);
        }

        public IdeaDto UpVote(IdeaDto idea)
        {
            ++idea.PositiveVotes;
            return Edit(idea);
        }

        public IdeaDto DownVote(IdeaDto idea)
        {
            ++idea.NegativeVotes;
            return Edit(idea);
        }

        public IEnumerable<IdeaDto> GetAllIdeasFromProject(ProjectDto projectDto)
        {
            var ideas =
                Gateway.Repositories.Idea.Query(true).Where(project => project.Project.Key == projectDto.Key);
            return Mapper.Map<IEnumerable<IdeaDto>>(ideas);
        }

        public IEnumerable<CommentDto> GetCommentsForIdea(IdeaDto idea)
        {
            var model = Mapper.Map<Idea>(idea);

            var comments = Gateway.Repositories.Comment.GetCommentsForIdea(model);

            return Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentDto>>(comments);
        } 

        public CommentDto AddCommentToIdea(CommentDto comment)
        {
            var model = Mapper.Map<Comment>(comment);

            var resComment = Gateway.Repositories.Comment.Insert(model);

            if (resComment == null) return null;

            var result = Gateway.Repositories.Idea.InsertComment(resComment);

            return result != null ? Mapper.Map<CommentDto>(result) : null;
        }

    }
}
