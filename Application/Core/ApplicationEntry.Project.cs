﻿using System.Linq;
using AutoMapper;
using DomainDTO.Implementations;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Entities;
using System;
using System.Collections.Generic;

namespace Application.Core
{
    public partial class ApplicationEntry
    {
        public IEnumerable<ProjectDto> GetAllProjects()
        {
            var projects = Gateway.Repositories.Project.Query();
            var projectsDto = Mapper.Map<IEnumerable<ProjectDto>>(projects);
            return projectsDto;
        }

        public ProjectDto Get(ProjectDto project)
        {
            var model = Mapper.Map<Project>(project);
            model.EnsureNotNull(new ArgumentNullException("project", "Project not valid, cannot be null"));

            var result = Gateway.Repositories.Project.Get(model, true);
            return result != null ? Mapper.Map<ProjectDto>(result) : null;
        }

        public ProjectDto Insert(ProjectDto project, UserDto user)
        {
            var model = Mapper.Map<Project>(project);
            var userEntity = Mapper.Map<User>(user);

            userEntity.EnsureNotNull("User should exist.");

            model.EnsureNotNull(new ArgumentNullException("project", "Project not valid, cannot be null"));

            var result = Gateway.Repositories.Project.Insert(model);
            Gateway.Repositories.Project.InserUserOnProject(userEntity, result);

            if (result == null) return null;

            Gateway.Services.ApiClient.CreateProject(result);

            return Mapper.Map<ProjectDto>(result);
        }

        public ProjectDto Edit(ProjectDto project)
        {
            var model = Mapper.Map<Project>(project);
            model.EnsureNotNull(new ArgumentNullException("project", "Project not valid, cannot be null"));

            var result = Gateway.Repositories.Project.Update(model);
            return result != null ? Mapper.Map<ProjectDto>(result) : null;
        }

        public bool Delete(ProjectDto project)
        {
            var model = Mapper.Map<Project>(project);
            model.EnsureNotNull(new ArgumentNullException("project", "Project not valid, cannot be null"));

            var result = Gateway.Repositories.Project.Delete(model);

            if (!result) return false;

            Gateway.Services.ApiClient.RemoveProject(model);

            return true;
        }

        public IEnumerable<ProjectDto> GetUserProjects(UserDto userDto)
        {
            var projects =
                Gateway.Repositories.Project.Query(true).Where(p => p.Users.Any(u => u.Id == userDto.Id.ToString()));
            var projectsDto = Mapper.Map<IEnumerable<ProjectDto>>(projects);
            return projectsDto;
        }

        public IEnumerable<CommentDto> GetCommentsForProject(ProjectDto project)
        {
            var model = Mapper.Map<Project>(project);

            var comments = Gateway.Repositories.Comment.GetCommentsForProject(model);

            return Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentDto>>(comments);
        }

        public CommentDto AddCommentToProject(CommentDto comment)
        {
            var model = Mapper.Map<Comment>(comment);

            model.EnsureNotNull(new ArgumentNullException("comment", "Comment not valid, cannot be null"));

            var resComment = Gateway.Repositories.Comment.Insert(model);

            if (resComment == null) return null;

            var result = Gateway.Repositories.Project.InsertComment(resComment);

            return result != null ? Mapper.Map<CommentDto>(result) : null;
        }
    }
}