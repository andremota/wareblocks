﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DomainDTO.Implementations;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Core
{
    public partial class ApplicationEntry
    {
        public IEnumerable<FeatureDto> GetAllFeatures()
        {
            return Gateway.Repositories.Feature.Query(true).Project().To<FeatureDto>();
        }

        public IEnumerable<FeatureDto> GetAllFeaturesFromProject(ProjectDto project)
        {
            var projectFeatures =
                Gateway.Repositories.Feature.Query(true).Where(feature => feature.Project.Key == project.Key);
            return Mapper.Map<IEnumerable<Feature>, IEnumerable<FeatureDto>>(projectFeatures);
        }

        public FeatureDto Get(FeatureDto feature)
        {
            var model = Mapper.Map<Feature>(feature);
            model.EnsureNotNull(new ArgumentNullException("feature", "Project feature not valid, cannot be null"));

            var result = Gateway.Repositories.Feature.Get(model, true);

            return result != null ? Mapper.Map<FeatureDto>(result) : null;
        }

        public FeatureDto Insert(FeatureDto feature)
        {
            var model = Mapper.Map<Feature>(feature);

            model.EnsureNotNull(new ArgumentNullException("feature", "Project feature not valid, cannot be null"));

            var result = Gateway.Repositories.Project.InsertFeature(model);

            if (result == null) return null;

            Gateway.Services.ApiClient.CreateOrUpdateProjectFeature(result);

            return Mapper.Map<FeatureDto>(result);
        }

        public FeatureDto Edit(FeatureDto feature)
        {
            var model = Mapper.Map<Feature>(feature);
            model.EnsureNotNull(new ArgumentNullException("feature", "Project feature not valid, cannot be null"));

            var result = Gateway.Repositories.Feature.Update(model);

            if (result == null) return null;

            Gateway.Services.ApiClient.CreateOrUpdateProjectFeature(result);

            return Mapper.Map<FeatureDto>(result);
        }

        public bool Delete(FeatureDto feature)
        {
            var model = Mapper.Map<Feature>(feature);
            model.EnsureNotNull(new ArgumentNullException("feature", "Project feature not valid, cannot be null"));

            var result = Gateway.Repositories.Feature.Delete(model);

            if (!result) return false;

            Gateway.Services.ApiClient.CreateOrUpdateProjectFeature(model);

            return true;
        }

        public ImageDto AddImage(FeatureDto feature, ImageDto image)
        {
            var featureModel = Mapper.Map<Feature>(feature);
            var imageModel = Mapper.Map<Image>(image);

            featureModel.EnsureNotNull(new ArgumentNullException("feature", "Project feature not valid, cannot be null"));
            imageModel.EnsureNotNull(new ArgumentNullException("image", "Image not valid, cannot be null"));

            return null;
        }

        public IEnumerable<CommentDto> GetCommentsForFeature(FeatureDto feature)
        {
            var model = Mapper.Map<Feature>(feature);

            var comments = Gateway.Repositories.Comment.GetCommentsForFeature(model);

            return Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentDto>>(comments);
        } 

        public CommentDto AddCommentToFeature(CommentDto comment)
        {
            var model = Mapper.Map<Comment>(comment);

            model.EnsureNotNull(new ArgumentNullException("comment", "Comment not valid, cannot be null"));

            var resComment = Gateway.Repositories.Comment.Insert(model);

            if (resComment == null) return null;

            var result = Gateway.Repositories.Feature.InsertComment(resComment);

            return result != null ? Mapper.Map<CommentDto>(result) : null;
        }

        public bool ProcessPayment(FeatureDto feature)
        {
            return false;
        }

        public bool GenerateTokens()
        {
            var maxTokensToGenerate = ConfigurationManager.AppSettings["MaxTokens"];
            if (String.IsNullOrEmpty(maxTokensToGenerate))
                return false;

            int maxTokens;
            int.TryParse(maxTokensToGenerate, out maxTokens);

            var featureList = Gateway.Repositories.Feature.GetFeaturesThatNeedTokens();
            if (featureList == null) return false;

            Parallel.ForEach(featureList, feature => GenerateTokensForFeature(feature.Key, maxTokens));
            return true;
        }

        public bool GenerateTokensForFeature(Guid featureId, int maxTokens)
        {
            var tokenList = new List<Token>();
            while (maxTokens-- > 0)
                tokenList.Add(Gateway.Factories.Token.Get());

            var feature = Gateway.Repositories.Feature.GetById(featureId);

            return Gateway.Repositories.Feature.AddTokensToFeature(feature, tokenList);
        }

        public void SendBatchTokensToApi(Guid featureId)
        {
            var feature = Gateway.Repositories.Feature.GetById(featureId, true);

            Gateway.Services.ApiClient.CreateOrUpdateProjectFeature(feature);
        }

        public void VoteFeature(Guid featureId, Guid userId, bool isLike)
        {
            var vote = Gateway.Factories.Vote.Get(userId, isLike);

            Gateway.Repositories.Feature.VoteOnFeature(featureId, vote);
        }

        public VoteDto GetFeatureVote(Guid userId, Guid featureId)
        {
            var vote = Gateway.Repositories.Feature.GetVote(userId, featureId);
            return vote != null ? Mapper.Map<VoteDto>(vote) : null;
        }
    }
}