﻿using Application.Configs;
using AutoMapper;
using DomainDTO;
using DomainDTO.Implementations;
using DomainModel.Configs.IOC;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using Ninject;

namespace Application.Core
{
    public partial class ApplicationEntry
    {
        private IGateway Gateway { get; set; }

        public ApplicationEntry()
        {
            MapperInitializer.InitializeMapper();
            Gateway = new StandardKernel(new BootstrapModule()).Get<IGateway>();
        }

        public SubscribeBetaDto Insert(SubscribeBetaDto betaDto)
        {
            var beta = Mapper.Map<SubscribeBeta>(betaDto);
            var result = Gateway.Repositories.SubscribeBeta.Insert(beta);
            return Mapper.Map<SubscribeBetaDto>(result);
        }
    }
}