﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AutoMapper;
using DomainDTO.Implementations;
using WeedCSharpClient;
using Image = DomainModel.Implementations.Entities.Image;

namespace Application.Core
{
    public partial class ApplicationEntry
    {

        public IEnumerable<ImageDto> AddImage(string fileName, Stream fileStream)
        {
            var writeResultList = Gateway.Repositories.Image.Insert(new Image()
            {
                InputStream = fileStream,
                FileName = fileName
            });

            return Mapper.Map<IEnumerable<ImageDto>>(writeResultList);
        }

        public ImageDto GetImage(string fid)
        {
            var writeResultList = Gateway.Repositories.Image.Query().FirstOrDefault(i => i.ImageId == fid);

            return Mapper.Map<ImageDto>(writeResultList);
        }

        public bool IsValidImage(Stream fileStream)
        {
            try
            {
                using (var bitmap = new Bitmap(fileStream))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
