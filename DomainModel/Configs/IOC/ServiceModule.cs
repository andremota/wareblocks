﻿using DomainModel.Implementations.Services;
using DomainModel.Interfaces.Services.APIServices;
using Ninject.Modules;

namespace DomainModel.Configs.IOC
{
    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IApiClient>().To<ApiClient>();
        }
    }
}
