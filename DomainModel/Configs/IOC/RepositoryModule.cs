﻿using DomainModel.Configs.EF;
using DomainModel.Implementations.Repositories;
using DomainModel.Interfaces.Repositories;
using DomainModel.UnitOfWork;
using Ninject.Modules;

namespace DomainModel.Configs.IOC
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            //Unit Of Work and DbContext wrapper
            Bind<IContext>().To<InternalDbContext<WareBlocksContext>>();
            Bind<IUnitOfWork>().To<EFUnitOfWork>();

            Bind<IProjectRepository>().To<ProjectRepository>();
            Bind<IFeatureRepository>().To<FeatureRepository>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IPermissionRepository>().To<PermissionRepository>();
            Bind<IImageRepository>().To<ImageRepository>();
            Bind<IUserFinanceRepository>().To<UserFinanceRepository>();
            Bind<IApiRepository>().To<ApiRepository>();
            Bind<ITokenRepository>().To<TokenRepository>();
            Bind<ICommentRepository>().To<CommentRepository>();
            Bind<IIdeaRepository>().To<IdeaRepository>();
            Bind<ISubscribeBetaRepository>().To<SubscribeBetaRepository>();
        }
    }
}