﻿using DomainModel.Validators;
using Ninject.Modules;

namespace DomainModel.Configs.IOC
{
    public class ValidationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IEntityValidation>().To<EntityValidation>();
        }
    }
}