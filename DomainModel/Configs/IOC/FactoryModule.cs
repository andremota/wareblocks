﻿using DomainModel.Implementations.Factories;
using DomainModel.Interfaces.Factories;
using Ninject.Modules;

namespace DomainModel.Configs.IOC
{
    public class FactoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProjectFactory>().To<ProjectFactory>();
            Bind<IFeatureFactory>().To<FeatureFactory>();
            Bind<IUserFactory>().To<UserFactory>();
            Bind<IPermissionFactory>().To<PermissionFactory>();
            Bind<ITokenFactory>().To<TokenFactory>();
            Bind<IImageFactory>().To<ImageFactory>();
            Bind<IUserFinanceFactory>().To<UserFinanceFactory>();
            Bind<IApiFactory>().To<ApiFactory>();
            Bind<IIdeaFactory>().To<IdeaFactory>();
            Bind<IVoteFactory>().To<VoteFactory>();
            Bind<ICommentFactory>().To<CommentFactory>();
        }
    }
}