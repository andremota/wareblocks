﻿using DomainModel.Implementations.RDO;
using DomainModel.Interfaces.RDO;
using Ninject.Modules;

namespace DomainModel.Configs.IOC
{
    public class BootstrapModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGateway>().To<Gateway>();
            Bind<IServices>().To<Services>();
            Bind<IFactories>().To<Factories>();
            Bind<IRepositories>().To<Repositories>();
        }
    }
}