﻿using System.Data.Entity.ModelConfiguration;
using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            Property(user => user.Name).IsRequired();
            Property(user => user.Password).IsRequired();

            HasKey(user => user.Id);

            HasMany(u => u.Projects).WithMany(p => p.Users);
            HasMany(user => user.Comments).WithOptional(comment => comment.User).WillCascadeOnDelete(true);
        }
    }
}