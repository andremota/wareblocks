using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class CommentConfiguration : DomainEntityTypeConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            Property(comment => comment.Content).IsRequired();
        }
    }
}
