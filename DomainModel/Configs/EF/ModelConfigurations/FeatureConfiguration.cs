﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class FeatureConfiguration : DomainEntityTypeConfiguration<Feature>
    {
        public FeatureConfiguration()
        {
            Property(feature => feature.Name).IsRequired();
            Property(feature => feature.Description).IsRequired();
            Property(feature => feature.Budget).IsRequired();
            Property(feature => feature.CurrentBudget).IsOptional();

            #region Navigation Properties

            HasMany(feature => feature.Comments).WithOptional(comment => comment.Feature);
            HasMany(feature => feature.Phases).WithOptional(phase => phase.ProjectFeature).WillCascadeOnDelete(true);
            HasMany(feature => feature.Images).WithRequired().WillCascadeOnDelete(true);
            HasMany(feature => feature.Tokens).WithRequired(token => token.Feature).WillCascadeOnDelete(true);
            HasMany(feature => feature.Votes).WithOptional(vote => vote.Feature).WillCascadeOnDelete(true);

            #endregion Navigation Properties
        }
    }
}