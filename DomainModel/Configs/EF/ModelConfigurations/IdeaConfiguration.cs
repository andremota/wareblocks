﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class IdeaConfiguration : DomainEntityTypeConfiguration<Idea>{
    
        public IdeaConfiguration()
        {
            Property(idea => idea.Name).IsRequired();
            //HasMany(idea => idea.Images);
            #region Navigation Properties

            HasMany(idea => idea.Comments).WithOptional(comment => comment.Idea);

            #endregion 

        }
    }
}

