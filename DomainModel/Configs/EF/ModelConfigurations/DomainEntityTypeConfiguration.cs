﻿using DomainModel.Interfaces.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    /// <summary>
    /// Base configuration class, defines the id property, common on all enities.
    /// </summary>
    public class DomainEntityTypeConfiguration<T> : EntityTypeConfiguration<T> where T : class, IEntity
    {
        protected DomainEntityTypeConfiguration()
        {
            HasKey(x => x.Key);
        }
    }
}