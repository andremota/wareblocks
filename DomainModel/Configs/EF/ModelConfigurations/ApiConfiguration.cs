﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class ApiConfiguration : DomainEntityTypeConfiguration<Api>
    {
        public ApiConfiguration()
        {
            Property(api => api.AppId).IsRequired();
            Property(api => api.EndPoint).IsRequired();
            Property(api => api.AppSecretHash).IsRequired();
        }
    }
}