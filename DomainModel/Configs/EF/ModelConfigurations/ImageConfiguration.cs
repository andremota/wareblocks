﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class ImageConfiguration : DomainEntityTypeConfiguration<Image>
    {
        public ImageConfiguration()
        {
            Property(image => image.ImageId).IsRequired();
            
            Ignore(image => image.FileName);
            Ignore(image => image.InputStream);
            Ignore(image => image.CurrentVersion);
            Ignore(image => image.Location);
            Ignore(image => image.FileId);
        }
    }
}