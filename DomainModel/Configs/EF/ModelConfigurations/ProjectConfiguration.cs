﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class ProjectConfiguration : DomainEntityTypeConfiguration<Project>
    {
        public ProjectConfiguration()
        {
            Property(project => project.Name).IsRequired();
            Property(project => project.Description).IsRequired();
            Property(project => project.TotalFinanced).IsRequired();

            #region Navigation Properties

            HasMany(project => project.Ideas).WithRequired(idea => idea.Project).WillCascadeOnDelete(true);
            HasMany(project => project.Features).WithRequired(feature => feature.Project).WillCascadeOnDelete(true);
            HasMany(project => project.Comments).WithOptional(comment => comment.Project);
            HasMany(project => project.Users).WithMany(user => user.Projects);
            HasMany(project => project.Apis).WithMany(api => api.Projects);
            
            #endregion Navigation Properties
        }
    }
}