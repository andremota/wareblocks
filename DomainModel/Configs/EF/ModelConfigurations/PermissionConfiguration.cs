﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class PermissionConfiguration : DomainEntityTypeConfiguration<Permission>
    {
        public PermissionConfiguration()
        {
            Property(permission => permission.Name).IsRequired();

            #region Navigation Properties

            HasMany(permission => permission.Users).WithMany(user => user.Permissions);

            #endregion Navigation Properties
        }
    }
}