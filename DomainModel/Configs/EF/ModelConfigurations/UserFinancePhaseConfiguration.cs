﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class UserFinancePhaseConfiguration : DomainEntityTypeConfiguration<UserFinancePhase>
    {
        public UserFinancePhaseConfiguration()
        {
            Property(userFinance => userFinance.Price).IsRequired();
        }
    }
}