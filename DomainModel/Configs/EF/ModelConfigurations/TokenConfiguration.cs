﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class TokenConfiguration : DomainEntityTypeConfiguration<Token>
    {
        public TokenConfiguration()
        {
            Property(token => token.Status).IsOptional();
            Property(token => token.Value).IsRequired();
        }
    }
}