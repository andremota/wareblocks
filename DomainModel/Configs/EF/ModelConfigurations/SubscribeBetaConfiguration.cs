﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Configs.EF.ModelConfigurations
{
    public class SubscribeBetaConfiguration : DomainEntityTypeConfiguration<SubscribeBeta>
    {
        public SubscribeBetaConfiguration()
        {
        }
    }
}