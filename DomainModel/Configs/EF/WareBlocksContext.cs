﻿using DomainModel.Configs.EF.ModelConfigurations;
using DomainModel.Implementations.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace DomainModel.Configs.EF
{
    public class WareBlocksContext : IdentityDbContext<User>
    {
        //static WareBlocksContext()
        //{
        //    Database.SetInitializer<WareBlocksContext>(null);
        //}

        #region Sets

        /// <summary>
        /// Gets or sets the <see cref="DbSet{TEntity}"/> of Ideas
        /// </summary>
        public DbSet<Idea> Ideas { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="DbSet{TEntity}"/> of Projects
        /// </summary>
        public DbSet<Project> Projects { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="DbSet{TEntity}"/> of Permissions
        /// </summary>
        public DbSet<Permission> Permissions { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="DbSet{TEntity}"/> of Features
        /// </summary>
        public DbSet<Feature> Features { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="DbSet{TEntity}"/> of Images
        /// </summary>
        public DbSet<Image> Images { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="DbSet{TEntity}"/> of Apis
        /// </summary>
        public DbSet<Api> Apis { get; set; }

        #endregion Sets

        /// <summary>
        /// Construtor for the EntitiesContext.
        /// </summary>
        public WareBlocksContext()
            : base("name=Entities")
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new PermissionConfiguration());
            modelBuilder.Configurations.Add(new ProjectConfiguration());
            modelBuilder.Configurations.Add(new FeatureConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserFinancePhaseConfiguration());
            modelBuilder.Configurations.Add(new ImageConfiguration());
            modelBuilder.Configurations.Add(new ApiConfiguration());
            modelBuilder.Configurations.Add(new TokenConfiguration());
            modelBuilder.Configurations.Add(new IdeaConfiguration());
            modelBuilder.Configurations.Add(new VoteConfiguration());
            modelBuilder.Configurations.Add(new CommentConfiguration());
            modelBuilder.Configurations.Add(new SubscribeBetaConfiguration());
        }
    }
}