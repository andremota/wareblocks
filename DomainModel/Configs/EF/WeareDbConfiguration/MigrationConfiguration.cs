﻿using System.Data.Entity.Migrations;

namespace DomainModel.Configs.EF.WeareDbConfiguration
{
    public class MigrationConfiguration : DbMigrationsConfiguration<WareBlocksContext>
    {
        public MigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(WareBlocksContext context)
        {
        }
    }
}