﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;

namespace DomainModel.Configs.EF.WeareDbConfiguration
{
    public class WareBlocksDbDropCreateConfiguration : DbConfiguration
    {
        public WareBlocksDbDropCreateConfiguration()
        {
            SetDefaultConnectionFactory(new LocalDbConnectionFactory("v11.0"));
            SetDatabaseInitializer(new TestDatabaseInitializer()); //Configuração para testes unitarios
            SetProviderServices(SqlProviderServices.ProviderInvariantName, SqlProviderServices.Instance);
        }
    }
}