﻿using System.Data.Entity;

namespace DomainModel.Configs.EF.WeareDbConfiguration
{
    public class TestDatabaseInitializer : CreateDatabaseIfNotExists<WareBlocksContext>
    {
        public override void InitializeDatabase(WareBlocksContext context)
        {
            base.InitializeDatabase(context);
        }
    }
}