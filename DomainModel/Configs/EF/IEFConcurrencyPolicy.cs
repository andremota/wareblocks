﻿using System.Data.Entity.Core.Objects;

namespace DomainModel.Configs.EF
{
    /// <summary>
    /// The policy rules to be aplied when a concurrency problem occurs.
    /// </summary>
    /// <typeparam name="TModel">The type of model used by a repository.</typeparam>
    public interface IEFConcurrencyPolicy<in TModel>
    {
        /// <summary>
        /// The aplication of the policy.
        /// </summary>
        /// <param name="context">The current context.</param>
        /// <param name="model">The model that was changed concurrently.</param>
        void ApplyPolicy(IContext context, TModel model);
    }

    internal class ClientWinsPolicy<TModel> : IEFConcurrencyPolicy<TModel>
    {
        public void ApplyPolicy(IContext context, TModel model)
        {
            context.GetObjectContext().Refresh(RefreshMode.ClientWins, model);
            context.SaveChanges();
        }
    }

    internal class StoreWinsPolicy<TModel> : IEFConcurrencyPolicy<TModel>
    {
        public void ApplyPolicy(IContext context, TModel model)
        {
            context.GetObjectContext().Refresh(RefreshMode.StoreWins, model);
            context.SaveChanges();
        }
    }
}