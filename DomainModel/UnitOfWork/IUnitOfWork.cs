﻿namespace DomainModel
{
    /// <summary>
    /// Addictional things to do when we want to  commit a transaction
    /// </summary>
    public delegate void OnCommitHandler();

    /// <summary>
    /// Addictional things to do when we want to rollback a transation
    /// </summary>
    public delegate void OnRollbackHandler();

    /// <summary>
    /// The unit of work deals with the underlying connection.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Event for commit operation.
        /// </summary>
        event OnCommitHandler OnCommit;

        /// <summary>
        /// Event for rollback operation.
        /// </summary>
        event OnRollbackHandler OnRollback;

        /// <summary>
        /// Commits changes to the underlying connection.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollsback the changes my within a transaction scope.
        /// </summary>
        void RollBack();

        /// <summary>
        /// Starts a transaction scope.
        /// </summary>
        /// <returns><c>IUnitOfWork</c> instance that wraps the transaction, representing a transaction scope.</returns>
        IUnitOfWork BeginTransaction();
    }
}