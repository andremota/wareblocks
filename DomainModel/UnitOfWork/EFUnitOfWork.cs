﻿using DomainModel.Configs.EF;
using DomainModel.Exceptions.ExeptionTransformation;
using System;
using System.Data;

namespace DomainModel.UnitOfWork
{
    public class EFUnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// DataBase context used
        /// </summary>
        private readonly IContext _context;

        private readonly IExceptionTransformation _exceptionTransformation;

        /// <summary>
        /// Represents the transaction used when accessing the database
        /// </summary>
        private IDbTransaction _transaction;

        private Int32 TransactionCount { get; set; }

        public EFUnitOfWork(IContext context, IExceptionTransformation exeptionTransformation)
        {
            _context = context;
            _exceptionTransformation = exeptionTransformation;
            TransactionCount = 0;
        }

        /// <summary>
        /// Handler called when the user commits the transactions
        /// </summary>
        public event OnCommitHandler OnCommit;

        /// <summary>
        /// Handler called when the user rollbacks the transactions
        /// </summary>
        public event OnRollbackHandler OnRollback;

        public void Commit()
        {
            TransactionCount--;

            InvokeOnCommit();

            // Verifies if this commit was made within a nested transaction.
            if (TransactionCount > 0) return;

            TransactionCount = 0;
            _transaction.Commit();
            CloseConnection();
        }

        public void RollBack()
        {
            InvokeOnRollback();

            // Verifies if transaction has been rolledback.
            if (TransactionCount == 0) return;

            TransactionCount = 0;
            _transaction.Rollback();

            CloseConnection();

            _context.ReloadContext();
        }

        public IUnitOfWork BeginTransaction()
        {
            TransactionCount++;

            if (TransactionCount > 1) return this;

            OpenConnection();

            _transaction = _context.BeginTransaction();

            return this;
        }

        /// <summary>
        /// Helper method to call the OnCommit event
        /// </summary>
        protected virtual void InvokeOnCommit()
        {
            var handler = OnCommit;
            if (handler != null) handler();
        }

        /// <summary>
        /// Helper method to call the OnRollBack event
        /// </summary>
        protected virtual void InvokeOnRollback()
        {
            var handler = OnRollback;
            if (handler != null) handler();
        }

        private void CloseConnection()
        {
            var entityConnection = _context.GetConnection();

            if (entityConnection.State != ConnectionState.Closed)
            {
                entityConnection.Close();
            }
        }

        private void OpenConnection()
        {
            try
            {
                var entityConnection = _context.GetConnection();

                if (entityConnection.State != ConnectionState.Open)
                {
                    entityConnection.Open();
                }
            }
            catch (Exception exception)
            {
                throw _exceptionTransformation.Transform(exception);
            }
        }
    }
}