﻿using System;

namespace DomainModel
{
    /// <summary>
    /// UnitOfWork Extension Methods.
    /// </summary>
    public static class UnitOfWorkExtensions
    {
        /// <summary>
        /// Executes an Action within a transaction scope. When an error occurs a rollback is made, otherwise changes are commited.
        /// </summary>
        /// <param name="unitOfWork">The <see cref="IUnitOfWork"/> instance.</param>
        /// <param name="action">The action with data changing behavior.</param>
        public static void WithinTransaction(this IUnitOfWork unitOfWork, Action<IUnitOfWork> action)
        {
            unitOfWork.WithinTransaction<Object>(unit =>
            {
                action(unit);
                return null;
            });
        }

        /// <summary>
        /// Executes a Function within a transaction scope. When an error occurs a rollback is made, otherwise changes are commited.
        /// </summary>
        /// <param name="unitOfWork">The <see cref="IUnitOfWork"/> instance.</param>
        /// <param name="func">The function with data changing behavior.</param>
        /// <typeparam name="TResult">The type of result returned by Func</typeparam>
        /// <returns>The result returned by func.</returns>
        public static TResult WithinTransaction<TResult>(this IUnitOfWork unitOfWork, Func<IUnitOfWork, TResult> func)
        {
            var trans = unitOfWork.BeginTransaction();

            try
            {
                var result = func(trans);

                trans.Commit();

                return result;
            }
            catch (Exception)
            {
                trans.RollBack();
                throw;
            }
        }
    }
}