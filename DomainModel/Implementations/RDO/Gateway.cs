﻿using DomainModel.Configs.IOC;
using DomainModel.Interfaces.Factories;
using DomainModel.Interfaces.RDO;
using DomainModel.Interfaces.Repositories;
using DomainModel.Interfaces.Services.APIServices;
using Ninject;

namespace DomainModel.Implementations.RDO
{
    public class Gateway : IGateway
    {
        private readonly StandardKernel _blocksKernel;

        public Gateway()
            : this(new StandardKernel(new BootstrapModule()))
        {
        }

        private Gateway(StandardKernel kernel)
        {
            _blocksKernel = kernel;
        }

        public IServices Services
        {
            get { return _blocksKernel.Get<IServices>(); }
        }

        public IFactories Factories
        {
            get { return _blocksKernel.Get<IFactories>(); }
        }

        public IRepositories Repositories
        {
            get { return _blocksKernel.Get<IRepositories>(); }
        }
    }

    public class Services : IServices
    {
        private readonly StandardKernel _servKernel;

        public Services()
            : this(new StandardKernel(new ServiceModule()))
        {
        }

        private Services(StandardKernel kernel)
        {
            _servKernel = kernel;
        }

        public IApiClient ApiClient { get { return _servKernel.Get<IApiClient>(); } }
    }

    public class Factories : IFactories
    {
        private readonly StandardKernel _facKernel;

        public Factories()
            : this(new StandardKernel(new FactoryModule()))
        {
        }

        private Factories(StandardKernel kernel)
        {
            _facKernel = kernel;
        }

        public IProjectFactory Project
        {
            get { return _facKernel.Get<IProjectFactory>(); }
        }

        public IFeatureFactory Feature
        {
            get { return _facKernel.Get<IFeatureFactory>(); }
        }

        public IUserFactory User
        {
            get { return _facKernel.Get<IUserFactory>(); }
        }

        public IUserFinanceFactory UserFinancePhase
        {
            get { return _facKernel.Get<IUserFinanceFactory>(); }
        }

        public IPermissionFactory Permission
        {
            get { return _facKernel.Get<IPermissionFactory>(); }
        }

        public ITokenFactory Token
        {
            get { return _facKernel.Get<ITokenFactory>(); }
        }

        public IImageFactory Image
        {
            get { return _facKernel.Get<IImageFactory>(); }
        }

        public IApiFactory Api
        {
            get { return _facKernel.Get<IApiFactory>(); }
        }

        public IIdeaFactory Idea
        {
            get { return _facKernel.Get<IIdeaFactory>(); }
        }

        public IVoteFactory Vote
        {
            get { return _facKernel.Get<IVoteFactory>(); }
        }

        public ICommentFactory Comment
        {
            get { return _facKernel.Get<ICommentFactory>(); }
        }
    }
}

public class Repositories : IRepositories
{
    private readonly StandardKernel _repKernel;

    public Repositories()
        : this(new StandardKernel(new RepositoryModule()))
    {
    }

    private Repositories(StandardKernel kernel)
    {
        _repKernel = kernel;
    }

    public IProjectRepository Project
    {
        get { return _repKernel.Get<IProjectRepository>(); }
    }

    public IFeatureRepository Feature
    {
        get { return _repKernel.Get<IFeatureRepository>(); }
    }

    public IUserRepository User
    {
        get { return _repKernel.Get<IUserRepository>(); }
    }

    public IUserFinanceRepository UserFinancePhase
    {
        get { return _repKernel.Get<IUserFinanceRepository>(); }
    }

    public IPermissionRepository Permission
    {
        get { return _repKernel.Get<IPermissionRepository>(); }
    }

    public IImageRepository Image
    {
        get { return _repKernel.Get<IImageRepository>(); }
    }

    public IApiRepository Api
    {
        get { return _repKernel.Get<IApiRepository>(); }
    }

    public ITokenRepository Token
    {
        get { throw new System.NotImplementedException(); }
    }

    public IIdeaRepository Idea
    {
        get { return _repKernel.Get<IIdeaRepository>(); }
    }

    public ICommentRepository Comment
    {
        get { return _repKernel.Get<ICommentRepository>(); }
    }

    public ISubscribeBetaRepository SubscribeBeta
    {
        get { return _repKernel.Get<ISubscribeBetaRepository>(); }
    }
}