﻿using System;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Exceptions.ExeptionTransformation;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System.Collections.Generic;

namespace DomainModel.Implementations.Repositories
{
    public class ImageRepository : EFRepository<Image>, IImageRepository
    {
        public ImageRepository(IContext entitiesContext)
            : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(Image model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Image, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Image, object>>[0];
        }
    }
}