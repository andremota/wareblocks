﻿using System;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System.Collections.Generic;

namespace DomainModel.Implementations.Repositories
{
    public class ApiRepository : EFRepository<Api>, IApiRepository
    {
        public ApiRepository(IContext entitiesContext)
            : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(Api model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Api, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Api, object>>[]
            {
                api => api.Projects
            };
        }
    }
}