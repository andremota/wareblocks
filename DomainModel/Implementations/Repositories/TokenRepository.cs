﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;

namespace DomainModel.Implementations.Repositories
{
    public class TokenRepository : EFRepository<Token>, ITokenRepository
    {
        public TokenRepository(IContext entitiesContext) : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(Token model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Token, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Token, object>>[]
            {
                token => token.Feature
            };
        }

        public Token CreateOrUpdate(Token token)
        {
            var t = Get(token);
            return t != null ? Update(t) : Insert(token);
        }
    }
}
