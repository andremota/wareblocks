﻿using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DomainModel.Implementations.Repositories
{
    public class SubscribeBetaRepository : EFRepository<SubscribeBeta>, ISubscribeBetaRepository
    {
        public SubscribeBetaRepository(IContext entitiesContext)
            : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(SubscribeBeta model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<SubscribeBeta, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<SubscribeBeta, object>>[]
            {
                beta => beta.Key
            };
        }
    }
}