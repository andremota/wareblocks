using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DomainModel.Implementations.Repositories
{
    public class UserRepository : IUserRepository
    {
        private UserManager<User> UserManager { get; set; }

        public UserRepository(UserManager<User> userManager)
        {
            UserManager = userManager;
        }

        public UserRepository()
            : this(new UserManager<User>(new UserStore<User>(new WareBlocksContext())))
        {
        }

        public IQueryable<User> Query(bool eagerLoad = false)
        {
            return UserManager.Users.AsQueryable();
        }

        public User GetById(Guid id, bool eagerLoad = false)
        {
            return UserManager.FindById(id.ToString());
        }

        public User Get(User model, bool eagerLoad = false)
        {
            return GetAsync(model).Result;
        }

        public async Task<User> GetAsync(User model)
        {
            return await UserManager.FindByIdAsync(model.Id);
        }

        public User Insert(User model)
        {
            return InsertAsync(model).Result;
        }

        public async Task<User> InsertAsync(User model)
        {
            var result = await UserManager.CreateAsync(model, model.Password);
            if (result.Succeeded)
            {
                //password won't be needed!
                model.Password = null;

                return model;
            }
            return null;
        }

        public User Update(User model)
        {
            return UpdateAsync(model).Result;
        }

        public async Task<User> UpdateAsync(User model)
        {
            var result = await UserManager.UpdateAsync(model);
            if (result.Succeeded)
            {
                return model;
            }
            return null;
        }

        public bool Delete(User model)
        {
            return DeleteAsync(model).Result;
        }

        public async Task<bool> DeleteAsync(User model)
        {
            return (await UserManager.DeleteAsync(model)).Succeeded;
        }

        public ClaimsIdentity GetUserIdentity(User user, string authType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            return GetUserIdentityAsync(user, authType).Result;
        }

        public async Task<ClaimsIdentity> GetUserIdentityAsync(User user, string authType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            return await UserManager.CreateIdentityAsync(user, authType);
        }

        public async Task<User> MatchUser(User user)
        {
            var userFound = await UserManager.FindAsync(user.UserName, user.Password);
            user.Password = null;
            return userFound;
        }
    }
}