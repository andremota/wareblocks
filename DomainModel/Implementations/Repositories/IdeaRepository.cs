﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;

namespace DomainModel.Implementations.Repositories
{
    public class IdeaRepository : EFRepository<Idea>, IIdeaRepository
    {
        public IdeaRepository(IContext entitiesContext) : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(Idea model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Idea, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Idea, object>>[]
            {
                idea => idea.Project
            };
        }

        public Comment InsertComment(Comment comment)
        {
            var userE = Context.Set<User>().Find(new object[] { comment.User.Id });
            var idea = GetById(comment.Idea.Key, true);
            
            if (idea == null) return null;
            if (userE == null) return null;

            comment.Idea = idea;
            comment.User = userE;

            idea.Comments.Add(comment);
            userE.Comments.Add(comment);

            Context.Set<Comment>().Add(comment);
            Context.Set<User>().Attach(userE);
            var res = Update(idea);

            return res != null ? comment : null;
        }


    }
}
