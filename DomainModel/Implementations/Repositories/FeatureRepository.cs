﻿using DomainModel.Configs.EF;
using DomainModel.Exceptions;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DomainModel.Implementations.Repositories
{
    public class FeatureRepository : EFRepository<Feature>, IFeatureRepository
    {
        private readonly ITokenRepository _tokenRepository;

        public FeatureRepository(IContext entitiesContext, ITokenRepository tokenRepository)
            : base(entitiesContext)
        {
            _tokenRepository = tokenRepository;
        }

        protected override IEnumerable<object> GetEntityKeyValues(Feature model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Feature, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Feature, object>>[]
            {
                feature => feature.Tokens,
                feature => feature.Project,
                                feature => feature.Votes
            };
        }

        public IEnumerable<Feature> GetFeaturesThatNeedTokens()
        {
            return Query(true).Where(feature => !feature.Tokens.Any());
        }

        public bool AddTokensToFeature(Feature feature, List<Token> tokenList)
        {
            var featureModel = Get(feature, true);

            if (tokenList == null || !tokenList.Any())
                throw new EntityNotFoundException("The list of entities [{0}] with identity: [{1}] was not found.", "Token", String.Join(", ", GetEntityKeyValues(feature)));

            foreach (var token in tokenList)
            {
                token.Feature = feature;
                featureModel.Tokens.Add(_tokenRepository.CreateOrUpdate(token));
            }

            var f = Update(featureModel);

            return f != null && f.Tokens.Any();
        }

        public void VoteOnFeature(Guid featureId, Vote vote)
        {
            var feature = GetById(featureId, true);
            vote.Feature = feature;
            Context.Set<Vote>().Add(vote);

            feature.Votes.Add(vote);
            Update(feature);
        }

        public Comment InsertComment(Comment comment)
        {
            var userE = Context.Set<User>().Find(new object[] { comment.User.Id });
            var feature = GetById(comment.Feature.Key, true);
            if (feature == null) return null;

            comment.Feature = feature;
            comment.User = userE;

            feature.Comments.Add(comment);
            userE.Comments.Add(comment);

            Context.Set<Comment>().Add(comment);
            Context.Set<User>().Attach(userE);
            var res = Update(feature);

            return res != null ? comment : null;
        }

        public Vote GetVote(Guid userId, Guid featureId)
        {
            throw new NotImplementedException();
        }
    }
}