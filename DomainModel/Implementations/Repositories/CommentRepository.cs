using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DomainModel.Implementations.Repositories
{
    public class CommentRepository : EFRepository<Comment>, ICommentRepository
    {
        public CommentRepository(IContext entitiesContext)
            : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(Comment model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Comment, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Comment, object>>[]
            {
                comment => comment.User,
                comment => comment.Idea,
                comment => comment.Feature,
                comment => comment.Project,
            };
        }

        public IEnumerable<Comment> GetCommentsForProject(Project project)
        {
            return Query(true).Where(comment => comment.Project.Key == project.Key);
        }

        public IEnumerable<Comment> GetCommentsForFeature(Feature feature)
        {
            return Query(true).Where(comment => comment.Feature.Key == feature.Key);
        }

        public IEnumerable<Comment> GetCommentsForIdea(Idea idea)
        {
            return Query(true).Where(comment => comment.Idea.Key == idea.Key);
        }
    }
}

