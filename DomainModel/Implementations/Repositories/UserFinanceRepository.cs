﻿using System;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Exceptions.ExeptionTransformation;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System.Collections.Generic;

namespace DomainModel.Implementations.Repositories
{
    public class UserFinanceRepository : EFRepository<UserFinancePhase>, IUserFinanceRepository
    {
        public UserFinanceRepository(IContext entitiesContext)
            : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(UserFinancePhase model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<UserFinancePhase, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<UserFinancePhase, object>>[0];
        }
    }
}