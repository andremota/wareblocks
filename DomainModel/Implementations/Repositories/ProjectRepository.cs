﻿using System;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System.Collections.Generic;

namespace DomainModel.Implementations.Repositories
{
    public class ProjectRepository : EFRepository<Project>, IProjectRepository
    {
        private readonly IApiRepository _apiRepository;

        public ProjectRepository(IContext entitiesContext, IApiRepository apiRepository)
            : base(entitiesContext)
        {
            _apiRepository = apiRepository;
        }

        protected override IEnumerable<object> GetEntityKeyValues(Project model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Project, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Project, object>>[]
            {
                project => project.Features,
                project => project.Apis,
                project => project.Ideas,
                project => project.Users
            };
        }

        public User InserUserOnProject(User user, Project projectToUpdate)
        {
            var userEntity = Context.Set<User>().Find(new object[]{user.Id});
            var project = GetById(projectToUpdate.Key, true); 
            
            if (project == null) return null;
            project.Users.Add(userEntity);

            var res = Update(project);

            return res != null ? user : null;
        }

        public Feature InsertFeature(Feature feature)
        {
            var project = GetById(feature.Project.Key, true);
            if (project == null) return null;

            project.Features.Add(feature);
            feature.Project = project;

            Context.Set<Feature>().Add(feature);
            var res = Update(project);

            return res != null ? feature : null;
        }

        public Idea InsertIdea(Idea idea)
        {
            var project = GetById(idea.Project.Key, true);
            if (project == null) return null;

            idea.Project = project;

            project.Ideas.Add(idea);

            Context.Set<Idea>().Add(idea);
            var res = Update(project);

            return res != null ? idea : null;
        }
        
        public Comment InsertComment(Comment comment)
        {
            var userE = Context.Set<User>().Find(new object[] { comment.User.Id });
            var project = GetById(comment.Project.Key, true);

            if (project == null) return null;

            comment.Project = project;
            comment.User = userE;

            project.Comments.Add(comment);
            userE.Comments.Add(comment);

            Context.Set<Comment>().Add(comment);
            Context.Set<User>().Attach(userE);
            var res = Update(project);

            return res != null ? comment : null;
        } 

        public Api InsertApi(Project project, Api api)
        {
            var p = GetById(project.Key, true);
            if (p == null) return null;

            var a = _apiRepository.Get(api, true);

            p.Apis.Add(api);
            a.Projects.Add(p);

            var res = Update(p);
            var res2 = _apiRepository.Update(a);

            return res != null && res2 != null ? a : null;
        }
    }
}