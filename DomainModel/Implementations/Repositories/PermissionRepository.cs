﻿using System;
using System.Linq.Expressions;
using DomainModel.Configs.EF;
using DomainModel.Exceptions.ExeptionTransformation;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Repositories;
using System.Collections.Generic;

namespace DomainModel.Implementations.Repositories
{
    public class PermissionRepository : EFRepository<Permission>, IPermissionRepository
    {
        public PermissionRepository(IContext entitiesContext)
            : base(entitiesContext)
        {
        }

        protected override IEnumerable<object> GetEntityKeyValues(Permission model)
        {
            yield return model.Key;
        }

        protected override IEnumerable<Expression<Func<Permission, object>>> GetForeignKeyValues()
        {
            return new Expression<Func<Permission, object>>[]
            {
                permission => permission.Users
            };
        }
    }
}