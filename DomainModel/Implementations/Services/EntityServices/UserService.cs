﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using DomainModel.Interfaces.Repositories;
using DomainModel.Interfaces.Services;
using System;
using System.Linq;

namespace DomainModel.Implementations.Services
{
    public class UserService : Service<IUserRepository>, IUserService
    {
        private readonly IGateway _gateway;

        public UserService(IUserRepository repository, IUnitOfWork unitOfWork, IGateway gateway)
            : base(repository, unitOfWork)
        {
            _gateway = gateway;
        }

        public IQueryable<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Permission> GetPermissions(User user)
        {
            throw new NotImplementedException();
        }

        public User AddPermission(User user, Permission permission)
        {
            throw new NotImplementedException();
        }
    }
}