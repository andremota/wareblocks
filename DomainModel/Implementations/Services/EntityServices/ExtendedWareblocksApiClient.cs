﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainModel.Exceptions;
using WareBlocks.API;

namespace DomainModel.Implementations.Services
{
    public class ExtendedWareblocksApiClient : WareBlocksApiClient
    {
        #region ctor
        public ExtendedWareblocksApiClient(string projectId, string projectSecret, string location)
            : base(projectId, projectSecret, location)
        {
        }
        #endregion

        #region Extended Api Operations
        public async Task CreateOrUpdateProjectFeatureAsync(string projectId, string featureId, IEnumerable<string> tokens)
        {
            var uri = BuildUri(string.Format("/project/{0}/feature/{1}", projectId, featureId));

            var toCreate = new
            {
                tokens
            };

            var response = await Update(uri, toCreate);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiOperationException(
                    string.Format("Could create project feature for the project {0} and feature {1} on the api {2}. HttpStatus: {3}",
                    projectId, featureId, uri, response.StatusCode), response.StatusCode);
            }

        }

        public async Task<dynamic> GetProjectFeature(string projectId, string projectFeatureId)
        {
            var uri = BuildUri(string.Format("/project/{0}/feature/{1}", projectId, projectFeatureId));

            var response = await Get(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiOperationException(
                    string.Format("Could not obtain project feature {0} of the project {1} for the api {2}. HttpStatus: {3}",
                    projectFeatureId, projectId, uri, response.StatusCode), response.StatusCode);
            }

            return await Deserialize<dynamic>(response);
        }

        public async Task RemoveProjectFeature(string projectId, string featureId)
        {
            var uri = BuildUri(string.Format("/project/{0}/feature/{1}", projectId, featureId));

            var response = await Delete(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiOperationException(
                    string.Format("Could not send tokens to the project feature {0} of the project {1} on the api {2}. HttpStatus: {3}",
                    featureId, projectId, uri, response.StatusCode), response.StatusCode);
            }
        }

        public async Task CreateProject(string projectId, string projectKey)
        {

            var uri = BuildUri("/project/");

            var toCreate = new
            {
                id = projectId,
                key = projectKey
            };

            var response = await Create(uri, toCreate);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiOperationException(
                    string.Format("Could not create project (id: {0}) on the given api (location: {1}). HttpStatus: {2}",
                    projectId, uri, response.StatusCode), response.StatusCode);
            }

        }

        public async Task<dynamic> GetProject(string id)
        {
            var uri = BuildUri(string.Format("/project/{0}", id));

            var response = await Get(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiOperationException(
                    string.Format("Could not obtain project {0} information for the api {1}. HttpStatus: {2}",
                    id, uri, response.StatusCode), response.StatusCode);
            }

            return await Deserialize<dynamic>(response);
        }

        public async Task RemoveProject(string projectId)
        {
            var uri = BuildUri(string.Format("/project/{0}", projectId));

            var response = await Delete(uri);

            if (!response.IsSuccessStatusCode)
            {
                throw new ApiOperationException(
                    string.Format("Could not create project (id: {0}) on the given api (location: {1}). HttpStatus: {2}",
                    projectId, uri, response.StatusCode), response.StatusCode);
            }
        }
        #endregion

        #region Helpers
        private string BuildUri(string path)
        {
            var uri = new UriBuilder(GetApiLocation())
            {
                Path = path
            };

            return uri.ToString();
        }
        #endregion
    }
}
