﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using DomainModel.Interfaces.Repositories;
using DomainModel.Interfaces.Services;
using System;
using System.Linq;

namespace DomainModel.Implementations.Services
{
    public class ProjectFeatureService : Service<IFeatureRepository>, IProjectFeatureService
    {
        private readonly IGateway _gateway;

        public ProjectFeatureService(IFeatureRepository repository, IUnitOfWork unitOfWork, IGateway gateway)
            : base(repository, unitOfWork)
        {
            _gateway = gateway;
        }

        public Feature CreateFeature(Feature feature)
        {
            throw new NotImplementedException();
        }

        public Feature UpdateFeature(Feature feature)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Image> GetImages(Feature feature)
        {
            throw new NotImplementedException();
        }

        public Image AddImage(Feature feature, Image image)
        {
            throw new NotImplementedException();
        }

        public IQueryable<UserFinancePhase> GetPhases(Feature feature)
        {
            throw new NotImplementedException();
        }

        public UserFinancePhase AddUserFinancePhase(Feature feature, UserFinancePhase phase)
        {
            throw new NotImplementedException();
        }
    }
}