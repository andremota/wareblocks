﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.RDO;
using DomainModel.Interfaces.Repositories;
using DomainModel.Interfaces.Services;
using System.Linq;

namespace DomainModel.Implementations.Services
{
    public class ProjectService : Service<IProjectRepository>, IProjectService
    {
        private readonly IGateway _gateway;

        public ProjectService(IProjectRepository repository, IUnitOfWork unitOfWork, IGateway gateway)
            : base(repository, unitOfWork)
        {
            _gateway = gateway;
        }

        public IQueryable<Project> GetProjects()
        {
            throw new System.NotImplementedException();
        }

        public Project CreateProject(Project project)
        {
            throw new System.NotImplementedException();
        }

        public Project UpdateProject(Project project)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Feature> GetProjectFeatures(Project project)
        {
            throw new System.NotImplementedException();
        }

        public Project AddFeature(Project project, Feature feature)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Api> GetApis(Project project)
        {
            throw new System.NotImplementedException();
        }
    }
}