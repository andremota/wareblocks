﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Services.APIServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModel.Implementations.Services
{
    #region Helper

    public static class ApiHelper
    {
        public static void ForEachApi<T>(this IEnumerable<Api> apis, T model, Action<T, Api> action)
        {
            if (apis != null)
            {
                foreach (var api in apis)
                {
                    action(model, api);
                }
            }
        }

        public static string ScapeKey(this Guid key)
        {
            return HttpServerUtility.UrlTokenEncode(key.ToByteArray());
        }
    }

    #endregion Helper

    #region ApiClient

    public class ApiClient : IApiClient
    {
        #region Ctors

        public string ProjectId { get; set; }

        public string ProjectKey { get; set; }

        /// <summary>
        /// Initializes the service with the api credentials.
        /// </summary>
        /// <param name="projectId">Project indentifier.</param>
        /// <param name="projectKey">Project secret key.</param>
        public ApiClient(string projectId, string projectKey)
        {
            ProjectId = projectId;
            ProjectKey = projectKey;
        }

        /// <summary>
        /// Reads the keys "WareblocksProjectId" and "WareblocksProjectSecretKey" from appSettings section for the project id and project key.
        /// </summary>
        public ApiClient()
            : this("1", "2")
        {
        }

        #endregion Ctors

        #region Client Creation

        private ExtendedWareblocksApiClient CreateClient(string location)
        {
            return new ExtendedWareblocksApiClient(ProjectId, ProjectKey, location);
        }

        #endregion Client Creation

        #region Api interaction

        public void CreateOrUpdateProjectFeature(Feature projectFeature, Api api)
        {
            var client = CreateClient(api.EndPoint);

            client.CreateOrUpdateProjectFeatureAsync(
                projectFeature.Project.Key.ScapeKey(),
                projectFeature.Key.ScapeKey(),
                projectFeature.Tokens == null
                    ? Enumerable.Empty<string>()
                    : projectFeature.Tokens.Select(t => t.Value)).Wait();
        }

        public void RemoveProjectFeature(Feature projectFeature, Api api)
        {
            var client = CreateClient(api.EndPoint);
            client.RemoveProjectFeature(projectFeature.Project.Key.ScapeKey(), projectFeature.Key.ScapeKey()).Wait();
        }

        public void CreateProject(Project project, Api api)
        {
            var client = CreateClient(api.EndPoint);
            client.CreateProject(project.Key.ScapeKey(), project.SecretKey).Wait();
        }

        public void RemoveProject(Project project, Api api)
        {
            var client = CreateClient(api.EndPoint);
            client.RemoveProject(project.Key.ScapeKey()).Wait();
        }

        #endregion Api interaction

        #region For All Apis

        public void CreateProject(Project project)
        {
            project.Apis.ForEachApi(project, CreateProject);
        }

        public void RemoveProject(Project project)
        {
            project.Apis.ForEachApi(project, RemoveProject);
        }

        public void CreateOrUpdateProjectFeature(Feature projectFeature)
        {
            projectFeature.Project.Apis.ForEachApi(projectFeature, CreateOrUpdateProjectFeature);
        }

        public void RemoveProjectFeature(Feature projectFeature)
        {
            projectFeature.Project.Apis.ForEachApi(projectFeature, RemoveProjectFeature);
        }

        #endregion For All Apis
    }

    #endregion ApiClient
}