using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Implementations.Factories
{
    public class CommentFactory : ICommentFactory
    {
        public Comment Get()
        {
            return new Comment();
        }
    }
}
