﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class ApiFactory : IApiFactory
    {
        public Api Get()
        {
            return new Api();
        }
    }
}