﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class ProjectFactory : IProjectFactory
    {
        public Project Get()
        {
            return new Project();
        }
    }
}