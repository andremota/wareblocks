﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class ImageFactory : IImageFactory
    {
        public Image Get()
        {
            return new Image();
        }
    }
}