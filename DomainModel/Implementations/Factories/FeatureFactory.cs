﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class FeatureFactory : IFeatureFactory
    {
        public Feature Get()
        {
            return new Feature();
        }
    }
}