﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class PermissionFactory : IPermissionFactory
    {
        public Permission Get()
        {
            return new Permission();
        }
    }
}