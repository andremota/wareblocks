﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class UserFactory : IUserFactory
    {
        public User Get()
        {
            return new User();
        }
    }
}