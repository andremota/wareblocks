﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class IdeaFactory : IIdeaFactory
    {
        public Idea Get()
        {
            return new Idea();
        }
    }
}
