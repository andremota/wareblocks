﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;

namespace DomainModel.Implementations.Factories
{
    public class UserFinanceFactory : IUserFinanceFactory
    {
        public UserFinancePhase Get()
        {
            return new UserFinancePhase();
        }
    }
}