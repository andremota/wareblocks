﻿using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;
using System;

namespace DomainModel.Implementations.Factories
{
    public class VoteFactory : IVoteFactory
    {
        public Vote Get(Guid userId, bool isLike)
        {
            return new Vote
            {
                Key = Guid.NewGuid(),
                UserId = userId,
                IsLike = isLike
            };
        }

        public Vote Get()
        {
            return new Vote
            {
                Key = Guid.NewGuid()
            };
        }
    }
}