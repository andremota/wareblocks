﻿using DomainModel.Enums;
using DomainModel.Implementations.Entities;
using DomainModel.Interfaces.Factories;
using System.Security.Cryptography;
using System.Text;

namespace DomainModel.Implementations.Factories
{
    public class TokenFactory : ITokenFactory
    {
        public Token Get()
        {
            return new Token
            {
                Value = GetUniqueKey(),
                Status = TokenStatus.Generated
            };
        }

        private string GetUniqueKey()
        {
            int maxSize = 8;
            var chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            var size = maxSize;
            var data = new byte[1];
            var crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            var result = new StringBuilder(size);

            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }

            return result.ToString();
        }
    }
}