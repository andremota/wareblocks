﻿using System.Collections.Generic;

namespace DomainModel.Implementations.Entities
{
    public class Permission : Entity
    {
        public string Name { get; set; }

        #region Navigation Properties

        public virtual ICollection<User> Users { get; set; }

        #endregion Navigation Properties
    }
}