﻿using System;
using System.Configuration;
using System.IO;
using ImageResizer;

namespace DomainModel.Implementations.Entities
{
    public class Image : Entity
    {

        #region Mapper Properties

        public string ImageId { get; set; }

        #endregion

        #region Not Mapper properties

        private const string ImageJobConfig = "ImageResizerJob_";
        public string FileName { get; set; }
        public Stream InputStream { get; set; }
        public int CurrentVersion { get; set; }

        public Uri Location { get; set; }
        public string FileId { get; set; }

        #endregion

        #region Image Helpers
        public Image GenerateVersionFromThis(int version)
        {
            return new Image()
            {
                InputStream = GenerateVersioning(InputStream, version),
                CurrentVersion = version,
                FileName = FileName,
                FileId = FileId
            };
        }

        /// <summary>
        /// This shouldn't be used, instead the GenerateVersionFromThis should be used on the Insertion of the original.
        /// <br/>
        /// When needed use GetOtherVersionLocation to get the location of the Image.
        /// </summary>
        /// <param name="original"></param>
        protected void GenerateCurrentVersionFromOriginalStream(Stream original)
        {
            InputStream = GenerateVersioning(original, CurrentVersion);
        }

        protected Stream GenerateVersioning(Stream from, int version)
        {
            from.Seek(0, SeekOrigin.Begin);
            var memoryStream = new MemoryStream();

            var jobConfig = ConfigurationManager.AppSettings[ImageJobConfig + version ];

            var job = new ImageJob(InputStream, memoryStream, new Instructions(jobConfig), false, false);

            ImageBuilder.Current.Build(job);

            memoryStream.Seek(0, SeekOrigin.Begin);
            from.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        public Image GetOtherVersionLocation(int version)
        {
            if (Location == null)
            {
                return null;
            }

            var builder = new UriBuilder(Location);

            var indexVersioning = Location.PathAndQuery.LastIndexOf('_');
            var originalPath = Location.PathAndQuery.Substring(0, indexVersioning + 1);

            builder.Path = originalPath + version;

            return new Image()
            {
                CurrentVersion = version,
                FileName = FileName,
                Location = builder.Uri,
                FileId = FileId
            };
        }

        ~Image()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (InputStream != null) InputStream.Dispose();
            }
        }
        #endregion
    }
}