﻿namespace DomainModel.Implementations.Entities
{
    public class UserFinancePhase : Entity
    {
        public decimal Price { get; set; }

        #region Navigation Properties

        public virtual Feature ProjectFeature { get; set; }

        #endregion Navigation Properties
    }
}