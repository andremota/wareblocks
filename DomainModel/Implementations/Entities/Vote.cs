﻿using System;

namespace DomainModel.Implementations.Entities
{
    public class Vote : Entity
    {
        public Guid UserId { get; set; }

        public bool IsLike { get; set; }

        public virtual Feature Feature { get; set; }
    }
}