﻿using DomainModel.Enums;

namespace DomainModel.Implementations.Entities
{
    public class Token : Entity
    {
        public string Value { get; set; }

        public TokenStatus Status { get; set; }

        #region Navigation Properties

        public virtual Feature Feature { get; set; }

        #endregion Navigation Properties
    }
}