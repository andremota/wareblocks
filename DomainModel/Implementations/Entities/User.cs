﻿using DomainModel.Interfaces.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace DomainModel.Implementations.Entities
{
    public class User : IdentityUser
    {
        public string Name { get; set; }

        /// <summary>
        /// This property is only used for the moment of the creation on an user.
        /// After the creation of the user this property shall be removed and a Hash shall be used (Property: PasswordHash).
        /// </summary>
        public string Password { get; set; }

        #region Navigation Properties

        public virtual ICollection<Permission> Permissions { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        #endregion Navigation Properties

    }
}