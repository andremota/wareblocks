namespace DomainModel.Implementations.Entities
{
    public class Comment : Entity
    {
        public string Content { get; set; }  

        #region Navigation Properties

        public virtual User User { get; set; }

        public virtual Idea Idea { get; set; }

        public virtual Feature Feature { get; set; }

        public virtual Project Project { get; set; }

        #endregion
    }
}

