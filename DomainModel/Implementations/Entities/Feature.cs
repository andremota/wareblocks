﻿using DomainModel.Enums;
using System.Collections.Generic;

namespace DomainModel.Implementations.Entities
{
    public class Feature : Entity
    {
        public FeatureType Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Budget { get; set; }

        public decimal CurrentBudget { get; set; }

        #region Navigation Properties

        public virtual ICollection<Image> Images { get; set; }

        public virtual Project Project { get; set; }

        public virtual ICollection<UserFinancePhase> Phases { get; set; }

        public virtual ICollection<Token> Tokens { get; set; }

        public virtual ICollection<Vote> Votes { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        #endregion Navigation Properties
    }
}