﻿namespace DomainModel.Implementations.Entities
{
    public class SubscribeBeta : Entity
    {
        public string ProjectName { get; set; }

        public string ProjectDescription { get; set; }

        public string Email { get; set; }
    }
}