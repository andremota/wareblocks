﻿using System.Collections.Generic;

namespace DomainModel.Implementations.Entities
{
    public class Api : Entity
    {
        public string AppId { get; set; }

        public string EndPoint { get; set; }

        public string AppSecretHash { get; set; }

        #region Navigation Properties

        public virtual ICollection<Project> Projects { get; set; }

        #endregion Navigation Properties
    }
}