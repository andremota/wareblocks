﻿using System.Collections.Generic;

namespace DomainModel.Implementations.Entities
{
    public class Project : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal TotalFinanced { get; set; }

        /// <summary>
        /// Secret key used to access the Api.
        /// </summary>
        public string SecretKey { get; set; }

        #region Navigation Properties

        public virtual ICollection<Feature> Features { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<Api> Apis { get; set; }

        public virtual ICollection<Idea> Ideas { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        #endregion Navigation Properties
    }
}