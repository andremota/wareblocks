﻿using System.Collections.Generic;

namespace DomainModel.Implementations.Entities
{
    public class Idea : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int PositiveVotes { get; set; }

        public int NegativeVotes { get; set; }

        #region Navigation Properties

        public virtual Project Project { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        //public virtual ICollection<Image> Images { get; set; }

        #endregion
    }
}
