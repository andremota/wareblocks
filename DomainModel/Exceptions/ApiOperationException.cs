﻿using System;
using System.Net;

namespace DomainModel.Exceptions
{
    public class ApiOperationException : Exception
    {
        private readonly HttpStatusCode _statusCode;
        public HttpStatusCode StatusCode { get { return _statusCode; } }

        public ApiOperationException(HttpStatusCode statusCode) : base("Could not perform operation on the specified api")
        {
            _statusCode = statusCode;
        }

        public ApiOperationException(string msg, HttpStatusCode statusCode) : base(msg)
        {
            _statusCode = statusCode;
        }

        public ApiOperationException(string msg, HttpStatusCode statusCode, Exception innerException) : base(msg, innerException)
        {
            _statusCode = statusCode;
        }
    }
}
