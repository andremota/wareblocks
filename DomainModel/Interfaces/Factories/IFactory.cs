﻿namespace DomainModel.Interfaces.Factories
{
    public interface IFactory<out TModel>
    {
        TModel Get();
    }
}