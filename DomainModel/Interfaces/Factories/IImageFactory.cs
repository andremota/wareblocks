﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IImageFactory : IFactory<Image>
    {
    }
}