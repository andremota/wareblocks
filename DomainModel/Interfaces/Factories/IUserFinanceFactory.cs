﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IUserFinanceFactory : IFactory<UserFinancePhase>
    {
    }
}