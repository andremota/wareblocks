﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IProjectFactory : IFactory<Project>
    {
    }
}