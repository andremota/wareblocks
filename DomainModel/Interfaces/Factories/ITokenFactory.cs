﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface ITokenFactory : IFactory<Token>
    {
    }
}