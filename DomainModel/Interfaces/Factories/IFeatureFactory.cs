﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IFeatureFactory : IFactory<Feature>
    {
    }
}