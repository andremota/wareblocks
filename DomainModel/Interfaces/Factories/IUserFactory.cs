﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IUserFactory : IFactory<User>
    {
    }
}