﻿using DomainModel.Implementations.Entities;
using System;

namespace DomainModel.Interfaces.Factories
{
    public interface IVoteFactory : IFactory<Vote>
    {
        Vote Get(Guid userId, bool isLike);
    }
}