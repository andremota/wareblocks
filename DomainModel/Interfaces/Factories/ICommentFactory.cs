using DomainModel.Implementations.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Interfaces.Factories
{
    public interface ICommentFactory : IFactory<Comment>
    {
    }
}
