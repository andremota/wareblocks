﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IApiFactory : IFactory<Api>
    {
    }
}