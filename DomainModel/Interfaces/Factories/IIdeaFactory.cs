﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IIdeaFactory : IFactory<Idea>
    {
    }
}
