﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Factories
{
    public interface IPermissionFactory : IFactory<Permission>
    {
    }
}