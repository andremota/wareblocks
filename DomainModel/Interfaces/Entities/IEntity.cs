﻿using System;

namespace DomainModel.Interfaces.Entities
{
    public interface IEntity
    {
        Guid Key { get; set; }
    }
}