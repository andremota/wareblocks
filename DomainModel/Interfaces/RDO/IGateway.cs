﻿using DomainModel.Interfaces.Factories;
using DomainModel.Interfaces.Repositories;
using DomainModel.Interfaces.Services;
using DomainModel.Interfaces.Services.APIServices;

namespace DomainModel.Interfaces.RDO
{
    /// <summary>
    /// WareBlock Root Domain Object
    /// To access the domain model methods it is needed to call one of this properties
    /// The domain model is separated in three fields, Services, Factories and Repositories
    /// </summary>
    public interface IGateway
    {
        IServices Services { get; }

        IFactories Factories { get; }

        IRepositories Repositories { get; }
    }

    /// <summary>
    /// WareBlocks Repositories
    /// They give access to CRUD operations on the entities
    /// </summary>
    public interface IRepositories
    {
        IProjectRepository Project { get; }

        IFeatureRepository Feature { get; }

        IUserRepository User { get; }

        IUserFinanceRepository UserFinancePhase { get; }

        IPermissionRepository Permission { get; }

        IImageRepository Image { get; }

        IApiRepository Api { get; }

        ITokenRepository Token { get; }

        IIdeaRepository Idea { get; }

        ICommentRepository Comment { get; }

        ISubscribeBetaRepository SubscribeBeta { get; }
    }

    /// <summary>
    /// WareBlocks Factories
    /// They return a instance of a domain entity
    /// </summary>
    public interface IFactories
    {
        IProjectFactory Project { get; }

        IFeatureFactory Feature { get; }

        IUserFactory User { get; }

        IUserFinanceFactory UserFinancePhase { get; }

        IPermissionFactory Permission { get; }

        ITokenFactory Token { get; }

        IImageFactory Image { get; }

        IApiFactory Api { get; }

        IIdeaFactory Idea { get; }

        IVoteFactory Vote { get; }

        ICommentFactory Comment { get; }
    }

    /// <summary>
    /// WareBlocks Services
    /// </summary>
    public interface IServices
    {
        IApiClient ApiClient { get; }
    }
}