﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface ISubscribeBetaRepository : IRepository<SubscribeBeta>
    {
    }
}