﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface IApiRepository : IRepository<Api>
    {
    }
}