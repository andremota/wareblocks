﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface IImageRepository : IRepository<Image>
    {
    }
}