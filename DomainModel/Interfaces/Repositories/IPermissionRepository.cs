﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface IPermissionRepository : IRepository<Permission>
    {
    }
}