﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface IUserFinanceRepository : IRepository<UserFinancePhase>
    {
    }
}