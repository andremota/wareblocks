﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface ITokenRepository : IRepository<Token>
    {
        Token CreateOrUpdate(Token token);
    }
}
