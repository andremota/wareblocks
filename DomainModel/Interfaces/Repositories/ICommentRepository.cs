using System.Collections.Generic;
using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IEnumerable<Comment> GetCommentsForProject(Project project);

        IEnumerable<Comment> GetCommentsForFeature(Feature feature);

        IEnumerable<Comment> GetCommentsForIdea(Idea idea);
    }
}
