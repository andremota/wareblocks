﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface IIdeaRepository : IRepository<Idea>
    {
        Comment InsertComment(Comment comment);
    }
}
