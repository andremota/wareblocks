﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Repositories
{
    public interface IProjectRepository : IRepository<Project>
    {
        Feature InsertFeature(Feature feature);

        Idea InsertIdea(Idea idea);

        User InserUserOnProject(User user, Project project);
        
        Comment InsertComment(Comment comment);
    }
}