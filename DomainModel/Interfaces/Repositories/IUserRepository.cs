﻿using System.Security.Claims;
using System.Threading.Tasks;
using DomainModel.Implementations.Entities;
using Microsoft.AspNet.Identity;

namespace DomainModel.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Het the user claims for the given user.
        /// </summary>
        /// <param name="user">The user that has the claims that shall be returned.</param>
        /// <param name="authType">Authentication type</param>
        /// <returns>Returns the user claims.</returns>
        ClaimsIdentity GetUserIdentity(User user, string authType);

        /// <summary>
        /// Het the user claims for the given user.
        /// </summary>
        /// <param name="user">The user that has the claims that shall be returned.</param>
        /// <param name="authType">Authentication type</param>
        /// <returns>Returns the user claims.</returns>
        Task<ClaimsIdentity> GetUserIdentityAsync(User user, string authType = DefaultAuthenticationTypes.ApplicationCookie);

        /// <summary>
        /// Fetches an instance of User based on the info on model
        /// </summary>
        /// <param name="model"></param>
        /// <returns>The User with the ID</returns>
        Task<User> GetAsync(User model);

        /// <summary>
        /// Persists the model
        /// </summary>
        /// <param name="model"></param>
        /// <returns>The inserted User</returns>
        Task<User> InsertAsync(User model);

        /// <summary>
        /// Deletes an existing model
        /// </summary>
        /// <param name="model"></param>
        /// <returns>true if the model was deleted successfully</returns>
        Task<bool> DeleteAsync(User model);

        Task<User> MatchUser(User model);
    }
}