﻿using DomainModel.Implementations.Entities;
using System;
using System.Collections.Generic;

namespace DomainModel.Interfaces.Repositories
{
    public interface IFeatureRepository : IRepository<Feature>
    {
        IEnumerable<Feature> GetFeaturesThatNeedTokens();

        bool AddTokensToFeature(Feature feature, List<Token> tokenList);

        void VoteOnFeature(Guid featureId, Vote vote);

        Comment InsertComment(Comment comment);

        Vote GetVote(Guid userId, Guid featureId);
    }
}