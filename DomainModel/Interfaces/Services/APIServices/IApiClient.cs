﻿using DomainModel.Implementations.Entities;

namespace DomainModel.Interfaces.Services.APIServices
{
    /// <summary>
    /// Api client wrapper.
    /// </summary>
    public interface IApiClient
    {
        /// <summary>
        /// Creates a project in all the apis of the given project.
        /// </summary>
        /// <param name="project">The project to create, which must have references to all its apis.</param>
        void CreateProject(Project project);

        /// <summary>
        /// Creates a project in the given api.
        /// </summary>
        /// <param name="project">The project to be created.</param>
        /// <param name="api">The api location.</param>
        void CreateProject(Project project, Api api);

        /// <summary>
        /// Removes a project in all the apis of the given project.
        /// </summary>
        /// <param name="project">The project to remove, which must have references to all its apis.</param>
        void RemoveProject(Project project);

        /// <summary>
        /// Removes a project from the given api.
        /// </summary>
        /// <param name="project">The project to be removed.</param>
        /// <param name="api">The api location.</param>
        void RemoveProject(Project project, Api api);

        /// <summary>
        /// The project feature to create.
        /// </summary>
        /// <param name="projectFeature">The Feature to create, which must have references its project and to all its apis.</param>
        void CreateOrUpdateProjectFeature(Feature projectFeature);

        /// <summary>
        /// Creates a Feature from the given api.
        /// </summary>
        /// <param name="projectFeature">The project feature to be created.</param>
        /// <param name="api">The api location.</param>
        void CreateOrUpdateProjectFeature(Feature projectFeature, Api api);

        /// <summary>
        /// The project feature to remove.
        /// </summary>
        /// <param name="projectFeature">The Feature to remove, which must have references its project and to all its apis.</param>
        void RemoveProjectFeature(Feature projectFeature);

        /// <summary>
        /// Removes a Feature from the given api.
        /// </summary>
        /// <param name="projectFeature">The Feature to remove.</param>
        /// <param name="api">The api location.</param>
        void RemoveProjectFeature(Feature projectFeature, Api api);
    }
}