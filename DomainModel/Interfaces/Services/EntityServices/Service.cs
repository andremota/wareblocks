﻿namespace DomainModel.Interfaces.Services
{
    /// <summary>
    /// Base type for all business logic layer implementations.
    /// <para>
    ///     Each implementation should not receive more than 1 Repository and 1 UnitOfWork.
    ///     However it should receive other dependencies, such as other Services Logic interfaces or other services.
    /// </para>
    /// </summary>
    /// <typeparam name="TRepository">A repository type.</typeparam>
    public abstract class Service<TRepository>
    {
        /// <summary>
        /// Gets the Repository instance received by constructor.
        /// </summary>
        protected TRepository Repository { get; private set; }

        /// <summary>
        /// Gets the UnitOfWork instance received by constructor.
        /// </summary>
        protected IUnitOfWork UnitOfWork { get; private set; }

        /// <summary>
        ///
        /// The constructor for <see cref="Services{TRepository}"/>
        ///
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="unitOfWork">An instance of <see cref="IUnitOfWork"/></param>
        protected Service(TRepository repository, IUnitOfWork unitOfWork)
        {
            Repository = repository;
            UnitOfWork = unitOfWork;
        }
    }
}