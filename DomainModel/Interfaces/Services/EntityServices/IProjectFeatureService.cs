﻿using DomainModel.Implementations.Entities;
using System.Linq;

namespace DomainModel.Interfaces.Services
{
    public interface IProjectFeatureService
    {
        Feature CreateFeature(Feature feature);

        Feature UpdateFeature(Feature feature);

        IQueryable<Image> GetImages(Feature feature);

        Image AddImage(Feature feature, Image image);

        IQueryable<UserFinancePhase> GetPhases(Feature feature);

        UserFinancePhase AddUserFinancePhase(Feature feature, UserFinancePhase phase);
    }
}