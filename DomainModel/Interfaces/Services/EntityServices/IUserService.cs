﻿using DomainModel.Implementations.Entities;
using System.Linq;

namespace DomainModel.Interfaces.Services
{
    public interface IUserService
    {
        IQueryable<User> GetUsers();

        IQueryable<Permission> GetPermissions(User user);

        User AddPermission(User user, Permission permission);
    }
}