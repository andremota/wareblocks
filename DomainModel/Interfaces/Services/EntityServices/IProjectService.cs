﻿using DomainModel.Implementations.Entities;
using System.Linq;

namespace DomainModel.Interfaces.Services
{
    public interface IProjectService
    {
        IQueryable<Project> GetProjects();

        Project CreateProject(Project project);

        Project UpdateProject(Project project);

        IQueryable<Feature> GetProjectFeatures(Project project);

        Project AddFeature(Project project, Feature feature);

        IQueryable<Api> GetApis(Project project);
    }
}