﻿namespace DomainModel.Enums
{
    public enum TokenStatus
    {
        Generated,
        Sent,
        Activated,
        Disabled
    }
}