﻿namespace DomainModel.Enums
{
    public enum FeatureType
    {
        Crowdfunding, Developed, InDevelopment
    }
}
