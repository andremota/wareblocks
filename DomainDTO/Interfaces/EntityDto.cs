﻿using System;

namespace DomainDTO.Interfaces
{
    public abstract class EntityDto
    {
        public Guid Key { get; set; }

        public DateTime CreationDateTime { get; set; }
    }
}