﻿namespace DomainDTO.Enums
{
    public enum TokenStatus
    {
        Generated,
        Sent,
        Activated,
        Disabled
    }
}