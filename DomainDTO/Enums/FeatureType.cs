﻿namespace DomainDTO.Enums
{
    public enum FeatureType
    {
        Crowdfunding, Developed, InDevelopment
    }
}
