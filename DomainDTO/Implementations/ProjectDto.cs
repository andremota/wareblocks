﻿using DomainDTO.Interfaces;
using System.Collections.Generic;

namespace DomainDTO.Implementations
{
    public class ProjectDto : EntityDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal TotalFinanced { get; set; }

        #region Authorization Properties

        public string ClientId { get; set; }

        public string ClientName { get; set; }

        public string ClientSecretHash { get; set; }

        #endregion Authorization Properties

        #region Navigation Properties

        public ICollection<FeatureDto> ProjectFeatures { get; set; }

        public ICollection<UserDto> Users { get; set; }

        public ICollection<CommentDto> Comments { get; set; }

        #endregion Navigation Properties
    }
}