﻿using DomainDTO.Interfaces;

namespace DomainDTO.Implementations
{
    public class SubscribeBetaDto : EntityDto
    {
        public string ProjectName { get; set; }

        public string ProjectDescription { get; set; }

        public string Email { get; set; }
    }
}