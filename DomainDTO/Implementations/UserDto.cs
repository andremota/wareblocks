﻿using System;
using System.Collections.Generic;

namespace DomainDTO.Implementations
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        #region Navigation Properties

        public ICollection<PermissionDto> Permissions { get; set; }

        #endregion Navigation Properties
    }
}