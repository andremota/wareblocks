﻿using DomainDTO.Interfaces;
using System.Collections.Generic;

namespace DomainDTO.Implementations
{
    public class ApiDto : EntityDto
    {
        public string AppId { get; set; }

        public string EndPoint { get; set; }

        public string AppSecretHash { get; set; }

        #region Navigation Properties

        public IEnumerable<ProjectDto> Project { get; set; }

        #endregion Navigation Properties
    }
}