﻿using DomainDTO.Enums;
using DomainDTO.Interfaces;

namespace DomainDTO.Implementations
{
    public class TokenDto : EntityDto
    {
        public string Value { get; set; }

        public TokenStatus Status { get; set; }
    }
}