﻿using DomainDTO.Interfaces;
using System.Collections.Generic;

namespace DomainDTO.Implementations
{
    public class PermissionDto : EntityDto
    {
        public string Name { get; set; }

        #region Navigation Properties

        public ICollection<UserDto> Users { get; set; }

        #endregion Navigation Properties
    }
}