﻿using DomainDTO.Interfaces;

namespace DomainDTO.Implementations
{
    public class UserFinancePhaseDto : EntityDto
    {
        public decimal Price { get; set; }

        #region Navigation Properties

        public FeatureDto Feature { get; set; }

        #endregion Navigation Properties
    }
}