using DomainDTO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainDTO.Implementations
{
    public class CommentDto : EntityDto
    {
        public string Content { get; set; }

        #region Navigation Properties

        public virtual UserDto User { get; set; }

        public virtual IdeaDto Idea { get; set; }

        public virtual FeatureDto Feature { get; set; }

        public virtual ProjectDto Project { get; set; }

        #endregion
    }
}
