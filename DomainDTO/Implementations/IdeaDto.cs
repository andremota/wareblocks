using DomainDTO.Interfaces;
using System.Collections.Generic;

namespace DomainDTO.Implementations
{
    public class IdeaDto : EntityDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int PositiveVotes { get; set; }
        public int NegativeVotes { get; set; }

        #region Navigation Properties

        public ProjectDto Project { get; set; }

        public ICollection<CommentDto> Comments { get; set; }

        #endregion
    }
}
