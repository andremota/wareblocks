﻿using DomainDTO.Interfaces;

namespace DomainDTO.Implementations
{
    public class ImageDto : EntityDto
    {
        public string ImageId { get; set; }
    }
}