﻿using DomainDTO.Enums;
using DomainDTO.Interfaces;
using System.Collections.Generic;

namespace DomainDTO.Implementations
{
    public class FeatureDto : EntityDto
    {
        public FeatureType Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Budget { get; set; }

        public decimal CurrentBudget { get; set; }

        #region Navigation Properties

        public ICollection<ImageDto> Images { get; set; }

        public ProjectDto Project { get; set; }

        public ICollection<UserFinancePhaseDto> Phases { get; set; }

        public ICollection<CommentDto> Comments { get; set; }

        #endregion Navigation Properties
    }
}