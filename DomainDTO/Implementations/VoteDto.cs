﻿using DomainDTO.Interfaces;
using System;

namespace DomainDTO.Implementations
{
    public class VoteDto : EntityDto
    {
        public Guid UserId { get; set; }

        public bool IsLike { get; set; }
    }
}